<?

include './mb.php';

cleanupThemeFiles();
rebuildThemeFiles();
rebuildThemeFiles(true);

dbQuery("ALTER TABLE `metatags` DROP INDEX `controller`", false);
dbQuery("ALTER TABLE `metatags` ADD `site_id` INT NOT NULL DEFAULT '1' AFTER `robots`", false);
dbQuery("ALTER TABLE `metatags` ADD UNIQUE (`controller`, `mode`, `path`, `site_id`)", false);

dbQuery("ALTER TABLE `variables` DROP INDEX `variable`", false);
dbQuery("ALTER TABLE `variables` ADD `site_id` INT NOT NULL DEFAULT '1' AFTER `date_updated`", false);
dbQuery("ALTER TABLE `variables` ADD UNIQUE (`variable`, `template`, `site_id`)", false);

/* CLONE METATAGS SETTINGS FROM THE ORIGIN SITE */
if (is_numeric($site_id) && $site_id > 1) {
    $metatags = dbQuery("SELECT * FROM `metatags` WHERE `site_id` = 1", false);
    foreach ($metatags as $item) {
        unset($item['record_num']);
        if (isset($site_id)) {
            $item['site_id'] = $site_id;
        }
        dbInsert('metatags', $item, true);
    }
}

/* CLONE VARIABLES FROM THE ORIGIN SITE */
if (is_numeric($site_id) && $site_id > 1) {
    $variables = dbQuery("SELECT * FROM `variables` WHERE `site_id` = 1", false);
    foreach ($variables as $item) {
        unset($item['record_num']);
        if (isset($site_id)) {
            $item['site_id'] = $site_id;
        }
        dbInsert('variables', $item, true);
    }
}

setMessage('Update completed!');

$headertitle = $title = 'Update 1.0.6';
getTemplate('template.overall_header.php');
getTemplate('template.403.php');
getTemplate('template.overall_footer.php');
