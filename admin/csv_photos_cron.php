<?

include('db.php');

ini_set('user_agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.9) Gecko/20071025 Firefox/24.0.0.9');
require('scrape_class.php');

if (file_exists("$basepath/admin/scrapers")) {
    $files = scandir("$basepath/admin/scrapers");
    foreach ($files as $file) {
        if ($file !== '.' && $file !== '..' && file_exists("$basepath/admin/scrapers/$file")) {
            include_once "$basepath/admin/scrapers/$file";
        }
    }
}

function parseUrl($url) {
    $r = "^(?:(?P<scheme>\w+)://)?";
    $r .= "(?:(?P<login>\w+):(?P<pass>\w+)@)?";
    $r .= "(?P<host>(?:(?P<subdomain>[-\w\.]+)\.)?" . "(?P<domain>[-\w]+\.(?P<extension>\w+)))";
    $r .= "(?::(?P<port>\d+))?";
    $r .= "(?P<path>[-\w/]*/(?P<file>\w+(?:\.\w+)?)?)?";
    $r .= "(?:\?(?P<arg>[\w=&]+))?";
    $r .= "(?:#(?P<anchor>\w+))?";
    $r = "!$r!";

    preg_match($r, $url, $out);

    return $out;
}

function scrapeGallery($url) {
    global $conf;

    $collection = array();
    $headers = get_headers($url);
    $parsed = parseUrl($url);

    foreach ($headers as $i) {
        if (stripos($i, 404) !== false) {
            //404 gallery, no point continueing.
            return false;
        }
        if (stripos($i, "Location") === 0) { // lookup for redirection ("Location" at the beginning of the string)
            $newLoc = trim(str_replace("Location: ", "", $i));
            if (stripos($newLoc, "http://") !== false || stripos($newLoc, "https://") !== false || stripos($newLoc, "//") !== false) {
                $url = $newLoc;
            } else {
                $url = "$parsed[scheme]://" . $parsed[host] . '/' . $newLoc;
            }
            $context = stream_context_create(array('http' => array('follow_location' => true)));
            break;
        }
    }

    $scraper = false;
    foreach ($conf['scrapers'] as $key => $domains) {
        foreach ($domains as $domain) {
            if (strpos($domain, $parsed['domain']) !== false) {
                $scraper = $key;
                break;
            }
        }
        if ($scraper !== false) {
            break;
        }
    }

    if ($scraper !== false && function_exists("scrape{$scraper}")) {

        $collection = call_user_func("scrape{$scraper}", $url);
    } else {

        $context = stream_context_create(array('http' => array('follow_location' => true)));
        $url_data = file_get_contents($url, false, $context);
        $html = str_get_html($url_data);

        $base = $html->find('base');
        $links = $html->find('a');

        $parsed_file = parseUrl($url);
        $files_path = ($base[0]->attr['href'] != "") ? $base[0]->attr['href'] : "$parsed[scheme]://" . $parsed_file['host'] . str_replace($parsed_file['file'], '', $parsed_file['path']);
        
        foreach ($links as $link) {
            if (stripos($link->href, ".jpg") !== false || stripos($link->href, ".jpeg") !== false) {
                if (stripos($link->href, 'http://') !== false || stripos($link->href, 'https://') !== false || substr($link->href, 0, 2) == '//') {
                    if (substr($link->href, 0, 2) == '//') {
                        $collection[] = $parsed['scheme'] . ':' . $link->href;
                    } else {
                        $collection[] = $link->href;
                    }
                } else {
                    if (substr($link->href, 0, 1) == '/') {
                        $collection[] = $parsed['scheme'] . '://' . $parsed['host'] . $link->href;
                    } else {
                        $collection[] = $files_path . $link->href;
                    }
                }
            }
        }
    }

    return $collection;
}

$counterPhotos = 0;
$counterGalleries = 0;

$queue = dbQuery("SELECT * FROM `csv_import_photos` WHERE `processing` = 0 ORDER BY `record_num` DESC", false);
if (is_array($queue) && count($queue) > 0) {
    dbQuery("UPDATE `status` SET `csv_import_running` = 1, `csv_import_total` = '" . count($queue) . "' WHERE `csv_import_total` = 0", false);
}
if (is_array($queue)) {
    foreach ($queue as $item) {
        $checkRecord = dbRow("SELECT * FROM `csv_import_photos` WHERE `record_num` = '" . (int) $item['record_num'] . "' AND `processing` = 0");
        if (is_array($checkRecord)) {
            dbUpdate('csv_import_photos', array('processing' => 1, 'record_num' => $item['record_num']));
            unset($paysite_insert_id);
            //check if paysite exists (if specified)	
            if ($item['paysite'] != "") {
                $item['paysite'] = trim($item['paysite']);
                $paysiteExists = dbQuery("SELECT `record_num` FROM `paysites` WHERE `name` LIKE '" . mysql_real_escape_string($item['paysite']) . "'", false);
                if (is_array($paysiteExists)) {
                    $paysite_insert_id = $paysiteExists[0]['record_num'];
                } else {
                    $paysite_insert_id = dbInsert('paysites', array('name' => $item['paysite']));
                }
            } else {
                $paysite_insert_id = (int) $item['default_paysite'];
            }

            if (strlen($item['plugurl']) > 10) {
                $content_dir = uniqid();
                $gallery_insert_id = dbInsert('content', array(
                    'title' => $item['title'],
                    'directory' => $content_dir,
                    'thumbnail' => '',
                    'description' => $item['desc'],
                    'keywords' => $item['keywords'],
                    'date_added' => 'NOW()',
                    'submitter' => $item['submitter'],
                    'ip' => '',
                    'approved' => 0,
                    'paysite' => $paysite_insert_id,
                    'plug_url' => $item['plugurl'],
                    'source_thumb_url' => $item['flv'],
                ), true, false, array('date_added'));
            } elseif (strlen($item['flv']) > 10) {
                $csv_dir = uniqid();
                $csv_path = "$basepath/csv_photos";
                $path = "$csv_path/$csv_dir";
                @mkdir($csv_path);
                @chmod($csv_path, 0777);
                @mkdir($path);
                @chmod($path, 0777);
                $files = scrapeGallery($item['flv']);
                print_r($files);
                if (is_array($files) && count($files) > 0) {
                    foreach ($files as $k) {
                        $context = stream_context_create(array('http' => array('follow_location' => true)));
                        $string = file_get_contents($k, false, $context);
                        if (strlen($string) > 0) {
                            file_put_contents("$path/$counterPhotos.jpg", $string);
                            chmod("$path/$counterPhotos.jpg", 0777);
                            $counterPhotos++;
                        }
                    }

                    if (is_dir("$basepath/csv_photos/$csv_dir")) {
                        $first = $csv_dir[0];
                        $second = $csv_dir[1];
                        $third = $csv_dir[2];
                        $forth = $csv_dir[3];
                        $fifth = $csv_dir[4];
                        @mkdir("$gallery_path");
                        @chmod("$gallery_path", 0777);
                        @mkdir("$gallery_path/$first");
                        @chmod("$gallery_path/$first", 0777);
                        @mkdir("$gallery_path/$first/$second");
                        @chmod("$gallery_path/$first/$second", 0777);
                        @mkdir("$gallery_path/$first/$second/$third");
                        @chmod("$gallery_path/$first/$second/$third", 0777);
                        @mkdir("$gallery_path/$first/$second/$third/$forth");
                        @chmod("$gallery_path/$first/$second/$third/$forth", 0777);
                        @mkdir("$gallery_path/$first/$second/$third/$forth/$fifth");
                        @chmod("$gallery_path/$first/$second/$third/$forth/$fifth", 0777);
                        echo "mv \"$basepath/csv_photos/$csv_dir\" \"$gallery_path/$first/$second/$third/$forth/$fifth/\"";
                        shell_exec("mv \"$basepath/csv_photos/$csv_dir\" \"$gallery_path/$first/$second/$third/$forth/$fifth/\"");
                        $finalArray = array();
                        $newFiles = scandir("$gallery_path/$first/$second/$third/$forth/$fifth/$csv_dir");
                        foreach ($newFiles as $j) {
                            if ($j != '.' && $j != '..') {
                                $finalArray[] = $j;
                            }
                        }
                        dbReconnect();
                        if (count($finalArray) > 0 && !is_array(dbRow("SELECT `record_num` FROM `content` WHERE `source_thumb_url` = '" . mysql_real_escape_string($item['flv']) . "'"))) {
                            $gallery_insert_id = dbInsert('content', array(
                                'title' => $item['title'],
                                'directory' => $csv_dir,
                                'thumbnail' => '',
                                'description' => $item['desc'],
                                'keywords' => $item['keywords'],
                                'date_added' => 'NOW()',
                                'submitter' => $item['submitter'],
                                'ip' => '',
                                'approved' => 0,
                                'paysite' => $paysite_insert_id,
                                'source_thumb_url' => $item['flv'],
                            ), true, false, array('date_added'));
                            $imagesImported = array();
                            foreach ($finalArray as $filename) {
                                $imagesImported[] = dbInsert('images', array('title' => '', 'filename' => $filename, 'gallery' => $gallery_insert_id));
                            }
                            dbUpdate('content', array('thumbnail' => $imagesImported[0], 'record_num' => $gallery_insert_id));
                        }
                        if (strlen($csv_dir) > 0 && is_dir($path)) {
                            shell_exec("rm -rf \"$path\"");
                        }
                        // create default thumbnail only
                        makeThumbs($csv_dir, 'content', true);
                        $counterGalleries++;
                    }
                }
            }

            if (isset($gallery_insert_id) && is_numeric($gallery_insert_id)) {
                // Find and attach niches to the imported gallery
                $content_body = $item['keywords'] . " " . $item['title'] . " " . $item['desc'] . " " . $item['paysite'];
                $content_body = mb_strtolower($content_body, 'UTF-8');
                $niches = dbQuery("SELECT `record_num`, `csv_match`, `name` FROM `niches`", false);
                if (is_array($niches)) {
                    foreach ($niches as $niche) {
                        $csv_matches = explode(',', $niche['csv_match']);
                        foreach ($csv_matches as $csv_match) {
                            if ($csv_match != "" && strpos($content_body, mb_strtolower($csv_match, 'UTF-8')) !== false) {
                                dbInsert('content_niches', array('content' => $gallery_insert_id, 'niche' => $niche['record_num']), true);
                            }
                        }
                    }
                }

                if (is_numeric($item['niche']) && $item['niche'] > 0) {
                    dbInsert('content_niches', array('content' => $gallery_insert_id, 'niche' => $item['niche']), true);
                }

                // Find and attach models to the imported gallery
                $modelsList = explode(',', $item['models']);
                if (count($modelsList) > 0) {
                    foreach ($modelsList as $modelName) {
                        $modelName = trim($modelName);
                        if (!empty($modelName)) {
                            $modelFound = dbRow("SELECT `record_num` FROM `models` WHERE `name` LIKE '" . mysql_real_escape_string($modelName) . "' OR `aka` LIKE '%" . mysql_real_escape_string($modelName) . "%'");
                            if (is_array($modelFound)) {
                                dbInsert('content_models', array('model' => $modelFound['record_num'], 'content' => $gallery_insert_id));
                            } else {
                                $modelsExisting = dbQuery("SELECT `name`, `aka`, `record_num` FROM `models` ORDER BY `name` ASC", false);
                                if (is_array($modelsExisting)) {
                                    foreach ($modelsExisting as $modelRecord) {
                                        similar_text($modelName, trim($modelRecord['name']), $sim);
                                        if ($sim > 85) {
                                            dbInsert('content_models', array('model' => $modelRecord['record_num'], 'content' => $gallery_insert_id), true);
                                        } else {
                                            $aka = explode(',', $modelRecord['aka']);
                                            foreach ($aka as $a) {
                                                similar_text($modelName, trim($a), $sim);
                                                if ($sim > 85) {
                                                    dbInsert('content_models', array('model' => $modelRecord['record_num'], 'content' => $gallery_insert_id), true);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // Remove entry from the csv import table
            dbDelete('csv_import_photos', array('record_num' => $item['record_num']));
            $countLeft = dbRow("SELECT COUNT(`record_num`) AS `count` FROM `csv_import_photos`");
            if ($countLeft['count'] == 0) {
                dbQuery("UPDATE `status` SET `csv_import_running` = 0, `csv_import_total` = 0", false);
            }
            unset($gallery_insert_id);
        }
    }
}
echo "\r\n\r\n$counterGalleries galleries ($counterPhotos photos) were imported.";
exit;
