<?

//get main thumbnail for a gallery in a particular size.
function getMainThumb($row, $size, $placeholder = '', $optional_first = false) {
    global $thumb_url, $thumb_path;

    if (is_numeric($row)) {
        $row = dbRow("SELECT `directory`, (SELECT `filename` FROM `images` WHERE `record_num` = `content`.`thumbnail` LIMIT 1) AS `thumbfile` FROM `content` WHERE `record_num` = '" . (int) $row . "'");
    }

    if ($optional_first === true && $row['thumbfile'] == "") {
        $image = dbRow("SELECT `filename` FROM `images` WHERE `gallery` = '$row[record_num]' ORDER BY `weight` ASC, `record_num` ASC LIMIT 1");
        $row['thumbfile'] = $image['filename'];
    }

    $subdir = $row['directory'][0] . '/' . $row['directory'][1] . '/' . $row['directory'][2] . '/' . $row['directory'][3] . '/' . $row['directory'][4] . '/' . $row['directory'] . '/' . $size;
    if (file_exists($thumb_path . '/' . $subdir . '/' . $row['thumbfile'])) {
        return $thumb_url . '/' . $subdir . '/' . $row['thumbfile'];
    } else {
        $sizes = explode('x', $size);
        return getPlaceholderUrl($placeholder, array('width' => $sizes[0], 'height' => $sizes[1]));
    }
}

//get all content for a particular gallery - both fullsize images and thumbnails of specified size - in an array
function getImagesInGallery($row, $size) {
    global $gallery_url, $picThumbSizeFull, $conf;

    $thumbs = array();
    if ($conf['gallery_photos_sort_mode'] === 'weight') {
        $result = dbQuery("SELECT * FROM `images` WHERE `gallery` = '$row[record_num]' ORDER BY `weight`", false);
    } else {
        $result = dbQuery("SELECT * FROM `images` WHERE `gallery` = '$row[record_num]' ORDER BY `filename`", false);
    }
    foreach ($result as $i) {
        $thumbs[] = array(
            'thumb' => getThumbUrl(array('file' => $i['filename'], 'dir' => $row['directory'], 'preset' => $size)),
            'fullsize' => getThumbUrl(array('file' => $i['filename'], 'dir' => $row['directory'], 'preset' => $picThumbSizeFull)),
            'original' => "$gallery_url/$row[directory]/$i[filename]",
        );
    }
    if ($conf['gallery_photos_sort_mode'] === 'filename') {
        natsort2d($thumbs);
    }

    return $thumbs;
}

//get number of images in a gallery
function getNumImagesInGallery($id) {
    $result = dbRow("SELECT COUNT(`record_num`) AS `counter` FROM `images` WHERE `gallery` = '$id'", true, 60);
    return $result['counter'];
}

function getChannelsNumGalleries($id) {
    $count = dbRow("SELECT COUNT(`content`.`record_num`) AS counter FROM `content` WHERE `content`.`record_num` IN(SELECT `content` FROM `content_niches` WHERE `niche` = '$id') AND `content`.`enabled` = 1", true, 60);
    return $count['counter'];
}

function getModelsNumGalleries($id) {
    $count = dbRow("SELECT COUNT(`content`) AS counter FROM `content_models` LEFT JOIN `content` ON `content`.`record_num` = `content_models`.`content` WHERE `content_models`.`model` = '$id' AND `content`.`enabled` = 1", true, 60);
    return $count['counter'];
}

//get main thumbnail for a channel in a particular size.
function getChannelsMainThumb($row, $size, $placeholder = '') {
    global $channels_path, $channels_url;

    if (is_numeric($row)) {
        $row = dbRow("SELECT `directory`, `filename` FROM `niches` WHERE `record_num` = '" . (int) $row . "'");
    }

    $dir = $row['directory'];
    $first = $dir[0];
    $second = $dir[1];
    $third = $dir[2];
    $forth = $dir[3];
    $fifth = $dir[4];

    if ($row['filename'] != "" && is_dir($channels_path . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir)) {
        return $channels_url . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir . "/" . $size . "/" . $row['filename'];
    } else {
        $sizes = explode('x', $size);
        return getPlaceholderUrl($placeholder, array('width' => $sizes[0], 'height' => $sizes[1]));
    }
}

//get main thumbnail for a paysite in a particular size.
function getPaysitesMainThumb($row, $size, $placeholder = '') {
    global $paysites_path, $paysites_url;

    if (is_numeric($row)) {
        $row = dbRow("SELECT `directory`, `filename` FROM `paysites` WHERE `record_num` = '" . (int) $row . "'");
    }

    $dir = $row['directory'];
    $first = $dir[0];
    $second = $dir[1];
    $third = $dir[2];
    $forth = $dir[3];
    $fifth = $dir[4];

    if ($row['filename'] != "" && is_dir($paysites_path . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir)) {
        return $paysites_url . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir . "/" . $size . "/" . $row['filename'];
    } else {
        $sizes = explode('x', $size);
        return getPlaceholderUrl($placeholder, array('width' => $sizes[0], 'height' => $sizes[1]));
    }
}

function getPaysitesNumGalleries($id) {
    $count = dbRow("SELECT COUNT(`record_num`) AS `counter` FROM `content` WHERE `paysite` = '$id' AND `enabled` = 1", false);
    return $count['counter'];
}

//get main thumbnail for the model in a particular size.
function getModelMainThumb($row, $size, $placeholder = '') {
    global $models_path, $models_url;

    if (is_numeric($row)) {
        $row = dbRow("SELECT `directory`, `filename` FROM `models` WHERE `record_num` = '" . (int) $row . "'");
    }

    $dir = $row['directory'];
    $first = $dir[0];
    $second = $dir[1];
    $third = $dir[2];
    $forth = $dir[3];
    $fifth = $dir[4];

    if ($row['filename'] != "" && is_dir($models_path . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir)) {
        return $models_url . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir . "/" . $size . "/" . $row['filename'];
    } else {
        $sizes = explode('x', $size);
        return getPlaceholderUrl($placeholder, array('width' => $sizes[0], 'height' => $sizes[1]));
    }
}

//get main thumbnail for the member in a particular size.
function getUserMainThumb($row, $size, $placeholder = '') {
    global $avatars_path, $avatars_url, $basepath;

    if (is_numeric($row)) {
        $row = dbRow("SELECT `directory`, `filename` FROM `users` WHERE `record_num` = '" . (int) $row . "'");
    }

    $dir = $row['directory'];
    $first = $dir[0];
    $second = $dir[1];
    $third = $dir[2];
    $forth = $dir[3];
    $fifth = $dir[4];

    if ($row['filename'] != "" && is_dir($avatars_path . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir)) {
        return $avatars_url . "/" . $first . "/" . $second . "/" . $third . "/" . $forth . "/" . $fifth . "/" . $dir . "/" . $size . "/" . $row['filename'];
    } else {
        $sizes = explode('x', $size);
        if ($placeholder != '' && file_exists("$basepath/$placeholder")) {
            $file = $placeholder;
        } else {
            $file = 'core/images/avatar.jpg';
        }
        return getPlaceholderUrl($file, array('width' => $sizes[0], 'height' => $sizes[1]));
    }
}

//get user banner
function getUserBanner($row) {
    global $banners_path, $banners_url;

    if (is_numeric($row)) {
        $row = dbRow("SELECT `banner` FROM `users` WHERE `record_num` = '" . (int) $row . "'");
    }

    if (file_exists("$banners_path/$row[banner]")) {
        return "$banners_url/$row[banner]";
    }
}

function getPlaceholderUrl($file = '', $args = array()) {
    global $thumb_path, $thumb_url, $basepath, $basehttp;

    $src_file_path = "$basepath/$file";
    $src_file_url = "$basehttp/$file";
    if ($file == '' || !file_exists($src_file_path)) {
        $src_file_path = "$basepath/core/images/placeholder.jpg";
        $src_file_url = "$basehttp/core/images/placeholder.jpg";
        if (!is_numeric($args['width']) || !is_numeric($args['height'])) {
            return $src_file_url;
        }
    }

    $file_parts = array_reverse(explode('/', $src_file_url));

    $dst_file_path = "$thumb_path/$args[width]x$args[height]/$file_parts[0]";
    $dst_file_url = "$thumb_url/$args[width]x$args[height]/$file_parts[0]";

    if (!file_exists($dst_file_path) || $args['reset'] === true) {
        @mkdir("$thumb_path");
        chmod("$thumb_path", 0777);
        @mkdir("$thumb_path/$args[width]x$args[height]");
        chmod("$thumb_path/$args[width]x$args[height]", 0777);
        chmod($src_file_path, 0777);
        makeImageThumbnail($src_file_path, $dst_file_path, $args['width'], $args['height']);
        chmod($dst_file_path, 0777);
    }

    return $dst_file_url;
}

//get difference between two dates
function datediff($datefrom, $dateto = null, $using_timestamps = false) {
    if ($dateto === null) {
        if ($using_timestamps) {
            $dateto = time();
        } else {
            $dateto = date('Y-m-d H:i:s');
        }
    }
    if (!$using_timestamps) {
        $datefrom = strtotime($datefrom, 0);
        $dateto = strtotime($dateto, 0);
    }
    $difference = $dateto - $datefrom; // Difference in seconds

    if ($difference < 86400 && $difference >= 3600) {
        $datediff = floor($difference / 3600);
        if ($datediff > 1) {
            $datediff = $datediff . ' hrs ago';
        } else {
            $datediff = $datediff . ' hr ago';
        }
    } elseif ($difference < 3600) {
        $datediff = floor($difference / 60);
        if ($datediff > 1) {
            $datediff = $datediff . ' mins ago';
        } else {
            $datediff = $datediff . ' min ago';
        }
    } else {
        $datediff = floor($difference / 86400);
        if ($datediff > 30) {
            $datediff = date('j', $datefrom) . ' ' . date('F', $datefrom);
        } else if ($datediff > 1) {
            $datediff = $datediff . ' days ago';
        } else {
            $datediff = $datediff . ' day ago';
        }
    }
    return $datediff;
}

function buildTags($var, $split = ',') {
    global $basehttp;
    $tags = explode(',', str_replace(', ', ',', $var));
    foreach ($tags as $i) {
        $i2 = str_replace(' ', '-', $i);
        $string .= "<a href=\"$basehttp/search/$i2/\">$i</a>$split ";
    }

    if (strlen($string) > 0) {
        return substr($string, 0, -2);
    } else {
        return '';
    }
}

function getRelatedGalleries($related, $template, $exclude, $amount) {
    global $conf;

    $string = mysql_real_escape_string($string);
    $cacheName = "related" . $_SERVER[REQUEST_URI];
    $cacheResult = getCache($cacheName);
    if ($cacheResult) {
        $results = $cacheResult;
    } else {
        if ($related) {
            $results = dbQuery("SELECT `content`.*, (SELECT `content_views`.`views` FROM `content_views` WHERE `content_views`.`content` = `content`.`record_num`) AS `views`, (SELECT `filename` FROM `images` WHERE `images`.`record_num` = `content`.`thumbnail`) AS `thumbfile` FROM `content` WHERE `content`.`enabled` = 1 AND `content`.`record_num` != '$exclude' AND `content`.`record_num` IN ($related) ORDER BY FIELD(`content`.`record_num`, $related) LIMIT 0, $amount", false);
        } else {
            $results = dbQuery("SELECT `content`.*, (SELECT `content_views`.`views` FROM `content_views` WHERE `content_views`.`content` = `content`.`record_num`) AS `views`, (SELECT `filename` FROM `images` WHERE `images`.`record_num` = `content`.`thumbnail`) AS `thumbfile` FROM `content` WHERE `content`.`enabled` = 1 AND `content`.`record_num` != '$exclude' ORDER BY `content`.`encoded_date` DESC LIMIT 0, $amount", false);
        }
        setCache($cacheName, $results, $conf['overall_cache_time']);
    }

    return $results;
}

function getBeingWatched($args = null) {
    global $conf;

    $amount = 30;
    if (is_numeric($args)) {
        $amount = (int) $args;
    } else if (is_numeric($args['amount'])) {
        $amount = (int) $args['amount'];
    }

    $cacheName = "beingWatched$amount";
    $cacheResult = getCache($cacheName);
    if ($cacheResult) {
        $results = $cacheResult;
    } else {
        $results = dbQuery("SELECT DISTINCT(`content`.`record_num`), `content`.*, (SELECT `content_views`.`views` FROM `content_views` WHERE `content_views`.`content` = `content`.`record_num`) AS `views`, (SELECT `images`.`filename` FROM `images` WHERE `images`.`record_num` = `content`.`thumbnail`) AS `thumbfile` FROM `content` LEFT JOIN `watch_log` ON `content`.`record_num` = `watch_log`.`content` WHERE `content`.`enabled` = 1 ORDER BY `watch_log`.`time` DESC LIMIT $amount", true, 60);
        setCache($cacheName, $results, $conf['overall_cache_time']);
    }

    return $results;
}

function buildAdminChannels($id) {
    global $basehttp;

    $string = '';
    $results = dbQuery("SELECT `niches`.`name`, `niches`.`record_num` FROM `niches`, `content_niches` WHERE `content_niches`.`content` = '$id' AND `content_niches`.`niche` = `niches`.`record_num` AND `niches`.`enabled` = 1 GROUP BY `niches`.`record_num` ORDER BY `name` ASC", false);
    if (is_array($results)) {
        foreach ($results as $row) {
            $string .= "<a href='$basehttp/admin/edit_niche.php?id=$row[record_num]'>$row[name]</a>, ";
        }
    }

    return substr($string, 0, -2);
}

//builds list of channels for specific content
function buildChannels($id, $mode = 'channels') {
    global $basehttp;

    $data = array();
    $results = dbQuery("SELECT `niches`.`name`, `niches`.`record_num` FROM `niches`, `content_niches` WHERE `content_niches`.`content` = '$id' AND `content_niches`.`niche` = `niches`.`record_num` AND `niches`.`enabled` = 1 GROUP BY `niches`.`record_num` ORDER BY `name` ASC");
    if (is_array($results)) {
        foreach ($results as $row) {
            if ($mode == 'channels') {
                $data[] = '<a href="' . $basehttp . "/channels/" . $row['record_num'] . "/" . str_replace(' ', '-', strtolower($row['name'])) . '">' . $row['name'] . '</a>';
            } else {
                $data[] .= '<a href="' . $basehttp . "/search/" . $row['record_num'] . "/" . str_replace(' ', '-', strtolower($row['name'])) . '">' . $row['name'] . '</a>';
            }
        }
    }

    return $data;
}

/**
 * Shows a list of existing channels
 * 
 * @global string $cache_path Cache path
 * @global string $basehttp URL of the site
 * @global string $basepath Server path to the site root
 * @param array $args Array of additional settings
 */
function showChannels($args = array()) {
    global $cache_path, $conf;

    if (!isset($conf['channels_tree_cache_time'])) {
        $conf['channels_tree_cache_time'] = 3600;
    }

    $content = '';
    $cached_rfile = $cache_path . '/channelList';
    if (file_exists($cached_rfile) && ((time() - $conf['channels_tree_cache_time']) < filemtime($cached_rfile))) {
        $farray = unserialize(file_get_contents($cached_rfile));
    } else {
        $dresult = dbQuery("SELECT * FROM `niches` WHERE `enabled` = 1 ORDER BY `name` ASC", false);
        if (is_array($dresult)) {
            foreach ($dresult as $drow) {
                if ($args['count'] === true) {
                    $count = dbRow("SELECT COUNT(`content_niches`.`content`) AS `count` FROM `content_niches` LEFT JOIN `content` ON `content_niches`.`content` = `content`.`record_num` WHERE `content_niches`.`niche` = '$drow[record_num]' AND `content`.`approved` = 2 AND `content`.`enabled` = 1", true, 300, "nicheCount$drow[record_num]", 'cache', array('key' => "nicheCount$drow[record_num]"));
                    if ($count['count'] > 0) {
                        $drow['count'] = $count['count'];
                    }
                }
                $farray[] = $drow;
            }
        }
        file_put_contents($cached_rfile, serialize($farray));
    }
    if (is_array($farray)) {
        $content .= $args['wrapper_prefix'];
        foreach ($farray as $row) {
            $link = generateUrl('channel', $row['name'], $row['record_num']);
            $content .= $args['item_prefix'] . "<a href='$link'>" . $args['name_prefix'] . $row['name'] . $args['name_suffix'] . "</a>";
            if ($args['count'] === true) {
                $content .=!empty($args['count_prefix']) ? $args['count_prefix'] : '<small class="count">';
                $content .= $row['count'];
                $content .=!empty($args['count_suffix']) ? $args['count_suffix'] : '</small>';
            }
            $content .= $args['item_suffix'];
        }
        $content .= $args['wrapper_suffix'];
    }

    return $content;
}

function truncate($text, $count = 50) {
    return strlen($text) >= $count ? mb_substr($text, 0, $count, 'utf-8') . "..." : $text;
}

function printTagCloud($tags, $args = array()) {
    global $basehttp;

    $content = '';
    if (count($tags) > 0) {
        arsort($tags);
        $max_size = 18; // max font size in pixels
        $min_size = 10; // min font size in pixels
        $max_qty = max(array_values($tags));
        $min_qty = min(array_values($tags));
        $spread = $max_qty - $min_qty;
        if ($spread == 0) {
            $spread = 1;
        }
        $step = ($max_size - $min_size) / ($spread);
        foreach ($tags as $key => $value) {
            $size = round($min_size + (($value - $min_qty) * $step));
            $color = isset($args['colors']["size_$size"]) ? 'color:' . $args['colors']["size_$size"] . ';' : '';
            $content .= '<a href="' . $basehttp . '/search/' . $key . '/" style="' . $color . 'font-size:' . $size . 'px">' . $key . '</a> ';
        }
    }

    return $content;
}

function tagCloud($args = array()) {
    global $conf;

    $tags = array();
    $results = dbQuery("SELECT * FROM `keywords` ORDER BY `amount` DESC LIMIT 0, 50", ($conf['overall_cache_time'] > 0), $conf['overall_cache_time']);
    if (is_array($results)) {
        foreach ($results as $row) {
            $tags[$row['word']] = $row['amount'];
        }
    }

    return printTagCloud($tags, $args);
}

function buildModels($id) {
    global $basehttp;

    $string = '';
    $results = dbQuery("SELECT `models`.`name`, `models`.`record_num` FROM `models`, `content_models` WHERE `content_models`.`content` = '$id' AND `content_models`.`model` = `models`.`record_num` GROUP BY `models`.`record_num` ORDER BY `name` ASC", false);
    $bad = array('?', '!', ' ', '&', '*', '$', '#', '@');
    $good = array('', '', '-', '', '', '', '', '');
    if (is_array($results)) {
        foreach ($results as $row) {
            $link = "$basehttp/models/" . strtolower(str_replace($bad, $good, $row['name'])) . "-" . $row['record_num'] . ".html";
            $string .= '<a href="' . $link . '"><strong>' . $row['name'] . '</strong></a>, ';
        }
    }

    return substr($string, 0, -2);
}

function buildAdminModels($id) {
    global $basehttp;

    $string = '';
    $results = dbQuery("SELECT `models`.`name`, `models`.`record_num` FROM `models`, `content_models` WHERE `content_models`.`content` = '$id' AND `content_models`.`model` = `models`.`record_num` GROUP BY `models`.`record_num` ORDER BY `name` ASC", false);
    if (is_array($results)) {
        foreach ($results as $row) {
            $string .= "<a href='$basehttp/admin/edit_model.php?id=$row[record_num]'>$row[name]</a>, ";
        }
    }

    return substr($string, 0, -2);
}

/**
 * Returns user uploaded contents
 * 
 * @param integer $userid User ID
 * @param array $args Array with function arguments: 
 * offset, limit, 
 * wrapper_prefix, wrapper_suffix, 
 * item_prefix, item_suffix, 
 * items_per_row, 
 * break_wrapper (when items_per_row is used, and items are divided into rows, wrapper_prefix and wrapper_suffix break rows)
 * @return string Formatted content
 */
function userRecentUploads($userid, $args = array()) {
    global $conf;

    $args += array(
        'offset' => 0,
        'limit' => 10,
    );

    $args['offset'] = (int) $args['offset'];
    $args['limit'] = (int) $args['limit'];

    $content = '';
    $results = dbQuery("SELECT `content`.*, (SELECT `content_views`.`views` FROM `content_views` WHERE `content_views`.`content` = `content`.`record_num`) AS `views`, (SELECT `images`.`filename` FROM `images` WHERE `images`.`record_num` = `content`.`thumbnail`) AS `thumbfile` FROM `content` WHERE `content`.`enabled` = 1 AND `content`.`submitter` = '$userid' ORDER BY `content`.`encoded_date` DESC LIMIT $args[offset], $args[limit]", true, 300);
    if (isset($args['get_results']) && $args['get_results'] === true) {
        return $results;
    }
    if (!is_array($results)) {
        if ($userid == $_SESSION['user']['record_num']) {
            return setMessage('You have no uploads yet!', 'alert', true);
        } else {
            return setMessage('User has no uploads yet!', 'alert', true);
        }
    } else {
        $i = 0;
        if ($args['wrapper_prefix'] != '') {
            $content .= $args['wrapper_prefix'];
        }
        foreach ($results as $row) {
            if (is_numeric($args['items_per_row']) > 0 && (($i++ % $conf['results_per_row']) == 0) && $args['break_wrapper'] === true) {
                $content .= $args['wrapper_suffix'] . $args['wrapper_prefix'];
            }
            ob_start();
            getTemplateItem('gallery_item.php', array('row' => $row));
            $content .= $args['item_prefix'];
            $content .= ob_get_contents();
            $content .= $args['item_suffix'];
            ob_end_clean();
        }
        if ($args['wrapper_suffix'] != '') {
            $content .= $args['wrapper_suffix'];
        }
    }

    return $content;
}

/**
 * Returns friends list for the given user
 * 
 * @param integer $userid User ID
 * @param array $args Array with function arguments: 
 * offset, limit, 
 * wrapper_prefix, wrapper_suffix, 
 * item_prefix, item_suffix, 
 * items_per_row, 
 * break_wrapper (when items_per_row is used, and items are divided into rows, wrapper_prefix and wrapper_suffix break rows)
 * @return string Formatted content
 */
function getUsersFriends($userid, $args = array()) {
    global $conf;

    $args += array(
        'offset' => 0,
        'limit' => 10,
    );

    $args['offset'] = (int) $args['offset'];
    $args['limit'] = (int) $args['limit'];

    $content = '';
    $results = dbQuery("SELECT `users`.`record_num`, `users`.`username`, `users`.`filename`, `users`.`directory` FROM `users_friends` RIGHT JOIN `users` ON `users`.`record_num` = `users_friends`.`friend_id` WHERE `users_friends`.`user_id` = '$userid' AND `users_friends`.`approved` = 1 ORDER BY `users`.`username` ASC LIMIT $args[offset], $args[limit]", false);
    if (isset($args['get_results']) && $args['get_results'] === true) {
        return $results;
    }
    if (!is_array($results)) {
        if ($userid == $_SESSION['user']['record_num']) {
            return setMessage('You have no friends yet!', 'alert', true);
        } else {
            return setMessage('This user has no friends yet!', 'alert', true);
        }
    } else {
        $i = 0;
        if ($args['wrapper_prefix'] != '') {
            $content .= $args['wrapper_prefix'];
        }
        foreach ($results as $row) {
            if (is_numeric($args['items_per_row']) > 0 && (($i++ % $conf['results_per_row']) == 0) && $args['break_wrapper'] === true) {
                $content .= $args['wrapper_suffix'] . $args['wrapper_prefix'];
            }
            ob_start();
            getTemplateItem('member_item.php', array('row' => $row));
            $content .= $args['item_prefix'];
            $content .= ob_get_contents();
            $content .= $args['item_suffix'];
            ob_end_clean();
        }
        if ($args['wrapper_suffix'] != '') {
            $content .= $args['wrapper_suffix'];
        }
    }

    return $content;
}

/**
 * Returns content for the given model
 * 
 * @param integer $model_id Model ID
 * @param array $args Array with function arguments: 
 * offset, limit, 
 * wrapper_prefix, wrapper_suffix, 
 * item_prefix, item_suffix, 
 * items_per_row, 
 * break_wrapper (when items_per_row is used, and items are divided into rows, wrapper_prefix and wrapper_suffix break rows)
 * @return string Formatted content
 */
function getModelsContent($model_id, $args = array()) {
    global $conf;

    $args += array(
        'offset' => 0,
        'items_in_row' => $conf['results_per_row'],
    );

    $args['offset'] = (int) $args['offset'];
    $args['limit'] = (int) $args['limit'];

    $content = '';
    $results = dbQuery("SELECT `content`.*, (SELECT `content_views`.`views` FROM `content_views` WHERE `content_views`.`content` = `content`.`record_num`) AS `views`, (SELECT `images`.`filename` FROM `images` WHERE `images`.`record_num` = `content`.`thumbnail`) AS `thumbfile` FROM `content` WHERE `content`.`enabled` = 1 AND `content`.`record_num` IN (SELECT `content` FROM `content_models` WHERE `model` = '$model_id')" . ($args['limit'] ? " LIMIT $args[offset], $args[limit]" : ''), false);
    if (isset($args['get_results']) && $args['get_results'] === true) {
        return $results;
    }
    if (!is_array($results)) {
        return setMessage('There is no content for this model yet.', 'alert', true);
    } else {
        $i = 0;
        if ($args['wrapper_prefix'] != '') {
            $content .= $args['wrapper_prefix'];
        }
        foreach ($results as $row) {
            if (is_numeric($args['items_per_row']) > 0 && (($i++ % $conf['results_per_row']) == 0) && $args['break_wrapper'] === true) {
                $content .= $args['wrapper_suffix'] . $args['wrapper_prefix'];
            }
            ob_start();
            getTemplateItem('gallery_item.php', array('row' => $row, 'args' => $args));
            $content .= $args['item_prefix'];
            $content .= ob_get_contents();
            $content .= $args['item_suffix'];
            ob_end_clean();
        }
        if ($args['wrapper_suffix'] != '') {
            $content .= $args['wrapper_suffix'];
        }
    }

    return $content;
}

/**
 * Returns featured content globally or for the specific group (niche, paysite)
 * 
 * @param integer $model_id Model ID
 * @param array $args Array with function arguments: 
 * niche, paysite,
 * offset, limit, 
 * wrapper_prefix, wrapper_suffix, 
 * item_prefix, item_suffix, 
 * items_per_row, 
 * display_mode ('block' or 'page', Default: 'block')
 * break_wrapper (when items_per_row is used, and items are divided into rows, wrapper_prefix and wrapper_suffix break rows)
 * @return string Formatted content
 */
function getFeaturedContent($args = array()) {
    global $conf;

    $args += array(
        'offset' => 0,
        'items_in_row' => $conf['results_per_row'],
        'display_mode' => 'block',
    );

    $args['offset'] = (int) $args['offset'];
    $args['limit'] = (int) $args['limit'];

    $content = '';
    $results = dbQuery("SELECT DISTINCT(`content_featured`.`content`), `content`.*, "
            . "(SELECT `content_views`.`views` FROM `content_views` WHERE `content_views`.`content` = `content`.`record_num`) AS `views`, "
            . "(SELECT `images`.`filename` FROM `images` WHERE `images`.`record_num` = `content`.`thumbnail`) AS `thumbfile` "
            . "FROM `content` "
            . "INNER JOIN `content_featured` ON `content`.`record_num` = `content_featured`.`content` "
            . "WHERE `content`.`enabled` = 1 "
            . (is_numeric($args['niche']) ? "AND `content_featured`.`niche` IN('$args[niche]', 0) " : '')
            . (is_numeric($args['paysite']) ? "AND `content_featured`.`paysite` IN('$args[paysite]', 0) " : '')
            . "ORDER BY "
            . (is_numeric($args['niche']) ? "FIELD(`content_featured`.`niche`, '$args[niche]', 0), " : '')
            . (is_numeric($args['paysite']) ? "FIELD(`content_featured`.`paysite`, '$args[paysite]', 0), " : '')
            . "`content`.`record_num` DESC "
            . ($args['limit'] ? "LIMIT $args[offset], $args[limit]" : ''), false);
    if (isset($args['get_results']) && $args['get_results'] === true) {
        return $results;
    }
    if (!is_array($results)) {
        if ($args['display_mode'] === 'page') {
            return setMessage('There is no content for this model yet.', 'alert', true);
        }
    } else {
        $i = 0;
        if ($args['wrapper_prefix'] != '') {
            $content .= $args['wrapper_prefix'];
        }
        foreach ($results as $row) {
            if (is_numeric($args['items_per_row']) > 0 && (($i++ % $conf['results_per_row']) == 0) && $args['break_wrapper'] === true) {
                $content .= $args['wrapper_suffix'] . $args['wrapper_prefix'];
            }
            ob_start();
            getTemplateItem('gallery_item.php', array('row' => $row));
            $content .= $args['item_prefix'];
            $content .= ob_get_contents();
            $content .= $args['item_suffix'];
            ob_end_clean();
        }
        if ($args['wrapper_suffix'] != '') {
            $content .= $args['wrapper_suffix'];
        }
    }

    return $content;
}

//builds the subsorting links used to sort searches by newest/mostviewed/etc, most viewed by last day, week, month and so forth.
function buildSubSortLinks($mode = 'default') {
    global $basehttp, $q, $channel_name, $paysite_name;

    $channel_name = clearString(mb_strtolower($channel_name, 'UTF-8'));
    $paysite_name = clearString(mb_strtolower($paysite_name, 'UTF-8'));

    $activeOption = 'All Time';
    if ($_GET['dateRange'] == 'day') {
        $activeOption = 'Last 24 hours';
        if ($mode == 'select') {
            $day = ' selected="selected"';
        } else {
            $day = ' class="active"';
        }
    } elseif ($_GET['dateRange'] == 'week') {
        $activeOption = 'Last Week';
        if ($mode == 'select') {
            $week = ' selected="selected"';
        } else {
            $week = ' class="active"';
        }
    } elseif ($_GET['dateRange'] == 'month') {
        $activeOption = 'Last Month';
        if ($mode == 'select') {
            $month = ' selected="selected"';
        } else {
            $month = ' class="active"';
        }
    } else {
        if ($mode == 'select') {
            $alltime = ' selected="selected"';
        } else {
            $alltime = ' class="active"';
        }
    }

    if ($_GET['sortby'] == 'views') {
        $activeOption = 'Most Viewed';
        if ($mode == 'select') {
            $views = ' selected="selected"';
        } else {
            $views = ' class="active"';
        }
    } elseif ($_GET['sortby'] == 'rating') {
        $activeOption = 'Top Rated';
        if ($mode == 'select') {
            $rating = ' selected="selected"';
        } else {
            $rating = ' class="active"';
        }
    } elseif ($_GET['sortby'] == 'relevancy') {
        $activeOption = 'Relevancy';
        if ($mode == 'select') {
            $score = ' selected="selected"';
        } else {
            $score = ' class="active"';
        }
    } elseif ($_GET['sortby'] == 'newest') {
        $activeOption = 'Most Recent';
        if ($mode == 'select') {
            $newest = ' selected="selected"';
        } else {
            $newest = ' class="active"';
        }
    } else {
        if ($_GET['mode'] == 'search') {
            $activeOption = 'Relevancy';
            if ($mode == 'select') {
                $score = ' selected="selected"';
            } else {
                $score = ' class="active"';
            }
        } elseif ($_GET['mode'] == 'channel') {
            $activeOption = 'Most Recent';
            if ($mode == 'select') {
                $newest = ' selected="selected"';
            } else {
                $newest = ' class="active"';
            }
        } elseif ($_GET['mode'] == 'paysite') {
            $activeOption = 'Most Recent';
            if ($mode == 'select') {
                $newest = ' selected="selected"';
            } else {
                $newest = ' class="active"';
            }
        } else {
            if ($mode == 'select') {
                $newest = ' selected="selected"';
            } else {
                $newest = ' class="active"';
            }
        }
    }

    $content = '';
    if ($_GET['mode'] == 'most-viewed') {
        if ($mode == 'default') {
            $content .= '<span class="selected">' . $activeOption . '</span>';
            $content .= '<span class="trigger icon"></span>';
            $content .= '<ul class="options-list" style="display: none;">';
            $content .= '<li' . $alltime . '><a href="' . $basehttp . '/most-viewed/">All Time</a></li>';
            $content .= '<li' . $day . '><a href="' . $basehttp . '/most-viewed/day/">Last 24 hours</a></li>';
            $content .= '<li' . $week . '><a href="' . $basehttp . '/most-viewed/week/">Last Week</a></li>';
            $content .= '<li' . $month . '><a href="' . $basehttp . '/most-viewed/month/">Last Month</a></li>';
            $content .= '</ul>';
        } else {
            $content .= '<select name="sortby">';
            $content .= '<option' . $alltime . ' value="' . $basehttp . '/most-viewed/">All Time</option>';
            $content .= '<option' . $day . ' value="' . $basehttp . '/most-viewed/day/">Last 24 hours</option>';
            $content .= '<option' . $week . ' value="' . $basehttp . '/most-viewed/week/">Last Week</option>';
            $content .= '<option' . $month . ' value="' . $basehttp . '/most-viewed/month/">Last Month</option>';
            $content .= '</select>';
        }
    } else if ($_GET['mode'] == 'top-rated') {
        if ($mode == 'default') {
            $content .= '<span class="selected">' . $activeOption . '</span>';
            $content .= '<span class="trigger icon"></span>';
            $content .= '<ul class="options-list" style="display: none;">';
            $content .= '<li' . $alltime . '><a href="' . $basehttp . '/top-rated/">All Time</a></li>';
            $content .= '<li' . $day . '><a href="' . $basehttp . '/top-rated/day/">Last 24 hours</a></li>';
            $content .= '<li' . $week . '><a href="' . $basehttp . '/top-rated/week/">Last Week</a></li>';
            $content .= '<li' . $month . '><a href="' . $basehttp . '/top-rated/month/">Last Month</a></li>';
            $content .= '</ul>';
        } else {
            $content .= '<select name="sortby">';
            $content .= '<option' . $alltime . ' value="' . $basehttp . '/top-rated/">All Time</option>';
            $content .= '<option' . $day . ' value="' . $basehttp . '/top-rated/day/">Last 24 hours</option>';
            $content .= '<option' . $week . ' value="' . $basehttp . '/top-rated/week/">Last Week</option>';
            $content .= '<option' . $month . ' value="' . $basehttp . '/top-rated/month/">Last Month</option>';
            $content .= '</select>';
        }
    } else if ($_GET['mode'] == 'search' && (!isset($_GET['type']) || $_GET['type'] == 'photos')) {
        if ($mode == 'default') {
            $content .= '<span class="selected">' . $activeOption . '</span>';
            $content .= '<span class="trigger icon"></span>';
            $content .= '<ul class="options-list" style="display: none;">';
            $content .= '<li' . $score . '><a href="' . $basehttp . '/search/' . $q . '/">Relevancy</a></li>';
            $content .= '<li' . $newest . '><a href="' . $basehttp . '/search/' . $q . '/newest/">Most Recent</a></li>';
            $content .= '<li' . $rating . '><a href="' . $basehttp . '/search/' . $q . '/rating/">Top Rated</a></li>';
            $content .= '<li' . $views . '><a href="' . $basehttp . '/search/' . $q . '/views/">Most Viewed</a></li>';
            $content .= '</ul>';
        } else {
            $content .= '<select name="sortby">';
            $content .= '<option' . $score . ' value="' . $basehttp . '/search/' . $q . '/">Relevancy</option>';
            $content .= '<option' . $newest . ' value="' . $basehttp . '/search/' . $q . '/newest/">Most Recent</option>';
            $content .= '<option' . $rating . ' value="' . $basehttp . '/search/' . $q . '/newest/">Top Rated</option>';
            $content .= '<option' . $views . ' value="' . $basehttp . '/search/' . $q . '/newest/">Most Viewed</option>';
            $content .= '</select>';
        }
    } else if ($_GET['mode'] == 'channel') {
        if ($mode == 'default') {
            $content .= '<span class="selected">' . $activeOption . '</span>';
            $content .= '<span class="trigger icon"></span>';
            $content .= '<ul class="options-list" style="display: none;">';
            $content .= '<li' . $newest . '><a href="' . $basehttp . '/channels/' . $_GET['channel'] . '/' . $channel_name . '/newest/">Most Recent</a></li>';
            $content .= '<li' . $rating . '><a href="' . $basehttp . '/channels/' . $_GET['channel'] . '/' . $channel_name . '/rating/">Top Rated</a></li>';
            $content .= '<li' . $views . '><a href="' . $basehttp . '/channels/' . $_GET['channel'] . '/' . $channel_name . '/views/">Most Viewed</a></li>';
            $content .= '</ul>';
        } else {
            $content .= '<select name="sortby">';
            $content .= '<option' . $newest . ' value="' . $basehttp . '/channels/' . $_GET['channel'] . '/' . $channel_name . '/newest/">Most Recent</option>';
            $content .= '<option' . $rating . ' value="' . $basehttp . '/channels/' . $_GET['channel'] . '/' . $channel_name . '/rating/">Top Rated</option>';
            $content .= '<option' . $views . ' value="' . $basehttp . '/channels/' . $_GET['channel'] . '/' . $channel_name . '/views/">Most Viewed</option>';
            $content .= '</select>';
        }
    } else if ($_GET['mode'] == 'paysite') {
        if ($mode == 'default') {
            $content .= '<span class="selected">' . $activeOption . '</span>';
            $content .= '<span class="trigger icon"></span>';
            $content .= '<ul class="options-list" style="display: none;">';
            $content .= '<li' . $newest . '><a href="' . $basehttp . '/paysites/' . $_GET['paysite'] . '/' . $paysite_name . '/newest/">Most Recent</a></li>';
            $content .= '<li' . $rating . '><a href="' . $basehttp . '/paysites/' . $_GET['paysite'] . '/' . $paysite_name . '/rating/">Top Rated</a></li>';
            $content .= '<li' . $views . '><a href="' . $basehttp . '/paysites/' . $_GET['paysite'] . '/' . $paysite_name . '/views/">Most Viewed</a></li>';
            $content .= '</ul>';
        } else {
            $content .= '<select name="sortby">';
            $content .= '<option' . $newest . ' value="' . $basehttp . '/paysites/' . $_GET['paysite'] . '/' . $paysite_name . '/newest/">Most Recent</option>';
            $content .= '<option' . $rating . ' value="' . $basehttp . '/paysites/' . $_GET['paysite'] . '/' . $paysite_name . '/rating/">Top Rated</option>';
            $content .= '<option' . $views . ' value="' . $basehttp . '/paysites/' . $_GET['paysite'] . '/' . $paysite_name . '/views/">Most Viewed</option>';
            $content .= '</select>';
        }
    }

    return $content;
}

function checkNav($item, $checkPager = false) {
    global $thisController;

    switch ($item) {
        case 'index':
            return ($thisController == 'index' && !$_GET['mode'] && ($checkPager === false || ($checkPager === true && !$_GET['page']))) ? true : false;
        case 'my-uploads':
            return ($thisController == 'index' && $_GET['mode'] == 'my-uploads') ? true : false;
        case 'most-recent':
            return ($thisController == 'index' && $_GET['mode'] == 'most-recent') ? true : false;
        case 'top-rated':
            return ($thisController == 'index' && $_GET['mode'] == 'top-rated') ? true : false;
        case 'most-viewed':
            return ($thisController == 'index' && $_GET['mode'] == 'most-viewed') ? true : false;
        case 'most-discussed':
            return ($thisController == 'index' && $_GET['mode'] == 'most-discussed') ? true : false;
        case 'random':
            return ($thisController == 'index' && $_GET['mode'] == 'random') ? true : false;
        case 'channels':
            return ($thisController == 'channels') ? true : false;
        case 'photos':
            return ($_GET['mode'] == 'photos') ? true : false;
        case 'models':
            return ($thisController == 'models') ? true : false;
        case 'model_bio':
            return ($thisController == 'model_bio') ? true : false;
        case 'members':
            return ($thisController == 'members') ? true : false;
        default:
            return false;
    }
}

function getUserNumGalleries($userid) {
    global $conf;

    $cacheName = "getUserNumGalleries$userid";
    if (!is_numeric($userid)) {
        return false;
    }

    $cacheResult = getCache($cacheName);
    if ($cacheResult) {
        $farray = $cacheResult;
    } else {
        $farray = dbRow("SELECT COUNT(`record_num`) AS `counter` FROM `content` WHERE `submitter` = '$userid' AND `enabled` = 1", true, 60);
        setCache($cacheName, $farray, $conf['overall_cache_time']);
    }

    return $farray['counter'];
}

function getUserNumFavorites($userid) {
    global $conf;

    $cacheName = "getUserNumFavorites$userid";
    if (!is_numeric($userid)) {
        return false;
    }

    $cacheResult = getCache($cacheName);
    if ($cacheResult) {
        $farray = $cacheResult;
    } else {
        $farray = dbRow("SELECT COUNT(`content`) AS `counter` FROM `favorites` WHERE `user` = '$userid'");
        setCache($cacheName, $farray, $conf['overall_cache_time']);
    }

    return $farray['counter'];
}

function getUserNumFriends($userid) {
    global $conf;

    $cacheName = "getUserNumFriends$userid";
    if (!is_numeric($userid)) {
        return false;
    }

    $cacheResult = getCache($cacheName);
    if ($cacheResult) {
        $farray = $cacheResult;
    } else {
        $farray = dbRow("SELECT COUNT(`record_num`) AS `counter` FROM `users_friends` WHERE `user_id` = '$userid' AND `approved` = 1", true, 60);
        setCache($cacheName, $farray, $conf['overall_cache_time']);
    }

    return $farray['counter'];
}

/**
 * Returns ads related with given paysite (ID or paysite data array)
 * 
 * @param mixed $paysite ID (integer) or paysite data (array)
 * @return array Paysite related ADs array
 */
function getAds($paysite) {
    $ads = array();
    
    if (is_numeric($paysite)) {
        $paysite = dbRow("SELECT * FROM `paysites` WHERE `record_num` = '$paysite'", true, 60);
    } else if (is_array($paysite)) {
        $paysite = dbRow("SELECT * FROM `paysites` WHERE `record_num` = '$paysite[record_num]'", true, 60);
    }
    for ($i = 0; $i <= 9; $i++) {
        if ($paysite["ad$i"] != "") {
            $ads["ad$i"] = $paysite["ad$i"];
        }
    }

    return $ads;
}

/**
 * Returns content record
 * 
 * @param int $id Content ID
 * @return array
 */
function getContent($id) {
    return dbRow("SELECT * FROM `content` WHERE `record_num` = '" . (int) $id . "' AND `approved` = 2 AND `enabled` = 1", true, 60);
}

/**
 * Returns requested content information
 * 
 * @param int $id ID of a content record
 * @param string $column Column name to return
 * 
 * @return string
 */
function getContentData($id, $column) {
    $content = getContent($id);
    return isset($content[$column]) ? $content[$column] : '';
}

/**
 * Returns paysite record
 * 
 * @param int $id Paysite ID
 * @return array
 */
function getPaysite($id) {
    return dbRow("SELECT * FROM `paysites` WHERE `record_num` = '" . (int) $id . "' AND `enabled` = 1", true, 60);
}

/**
 * Returns requested paysite information
 * 
 * @param int $id ID of a paysite record
 * @param string $column Column name to return
 * 
 * @return string
 */
function getPaysiteData($id, $column) {
    $paysite = getPaysite($id);
    return isset($paysite[$column]) ? $paysite[$column] : '';
}

/**
 * Returns model record
 * 
 * @param int $id Model ID
 * @return array
 */
function getModel($id) {
    return dbRow("SELECT * FROM `models` WHERE `record_num` = '" . (int) $id . "'", true, 60);
}

/**
 * Returns requested model information
 * 
 * @param int $id ID of a model record
 * @param string $column Column name to return
 * 
 * @return string
 */
function getModelData($id, $column) {
    $model = getModel($id);
    return isset($model[$column]) ? $model[$column] : '';
}

/**
 * Returns niche record
 * 
 * @param int $id Niche ID
 * @return array
 */
function getNiche($id) {
    return dbRow("SELECT * FROM `niches` WHERE `record_num` = '" . (int) $id . "'", true, 60);
}

/**
 * Returns requested niche information
 * 
 * @param int $id ID of a niche record
 * @param string $column Column name to return
 * 
 * @return string
 */
function getNicheData($id, $column) {
    $niche = getNiche($id);
    return isset($niche[$column]) ? $niche[$column] : '';
}

/**
 * Returns user record
 * 
 * @param int $id User ID
 * @return array
 */
function getUser($id) {
    $user = dbRow("SELECT * FROM `users` WHERE `record_num` = '" . (int) $id . "' AND `enabled` = 1", true, 60);
    unset($user['password'], $user['salt']);
    return $user;
}

/**
 * Returns requested user information
 * 
 * @param int $id ID of a user record
 * @param string $column Column name to return
 * 
 * @return string
 */
function getUserData($id, $column) {
    $user = getUser($id);
    return isset($user[$column]) ? $user[$column] : '';
}
