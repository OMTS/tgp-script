<?

header('content-type:text/html;charset=utf-8');
ini_set('session.gc_maxlifetime', 86400);
session_set_cookie_params(86400);

session_start();

include('config.php');
include('config.api.php');

if (!isset($site_id)) {
    $site_id = 1;
}

include($basepath . "/admin/functions.licensing.php"); //general functions 
include($basepath . "/admin/functions.general.php"); //general functions 
include($basepath . "/admin/functions.mb.php"); //general functions 
include($basepath . "/admin/functions.templating.php"); //template functions 
include($basepath . "/admin/functions.photos.php"); //photos functions 
include($basepath . "/admin/functions.email.php"); //admin related functions
include($basepath . "/admin/functions.bootstrap.php"); //cms bootstrap file
include($basepath . "/admin/functions.custom.php"); //custom functions (please put all new functions in here)
if ($enable_twitter_login) {
    include($basepath . '/includes/twitter/twitter_init.php');
}