<?
$_mtime = explode(" ", microtime());
$_starttime = $_mtime[1] + $_mtime[0];
require_once('db.php');
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <title>Mechbunny TGP Admin Area</title>
        <meta name="keywords" content="" />
        <meta name="description" content="" />

        <link rel="shortcut icon" href="<? echo $basehttp; ?>/admin/public/favicon.ico" type="image/x-icon">  
        <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&amp;subset=latin,latin-ext">
        <link rel="stylesheet" href="<? echo $basehttp; ?>/admin/js/jcrop/jquery.crop.css">
        <link rel="stylesheet" href="<? echo $basehttp; ?>/admin/public/css/app.css" media="all">

        <!-- jQuery -->
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery-ui-1.10.4.custom.min.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.ui.touch-punch.min.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/responsive-tables.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.uniform.min.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.colorbox-min.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.mousewheel.min.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.mCustomScrollbar.min.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/jquery.timepicker.js"></script>
        <script src="<? echo $basehttp; ?>/admin/js/jcrop/jquery.crop.js"></script>

        <!-- libs -->
        <script src="<? echo $basehttp; ?>/admin/public/js/libs/modernizr.custom.min.js"></script>
        <!-- application -->
        <script src="<? echo $basehttp; ?>/admin/public/js/app/app.js"></script>
        <script src="<? echo $basehttp; ?>/admin/public/js/app/main.js"></script>

        <script src="<? echo $basehttp; ?>/includes/ckeditor/ckeditor.js"></script>
        <script src="<? echo $basehttp; ?>/includes/ckeditor/adapters/jquery.js"></script>

        <!-- Media Queries support for IE6-8 -->  
        <!--[if lt IE 9]><script src="<? echo $basehttp; ?>/admin/public/js/libs/respond.min.js"></script><![endif]-->
        <!-- HTML5 element support for IE6-8 -->
        <!--[if lt IE 9]><script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
        <!--[if (gte IE 6)&(lte IE 8)]>
            <script src="<? echo $basehttp; ?>/admin/public/js/libs/selectivizr-min.js"></script>
        <![endif]-->

        <script>
            var current_url = '<? echo $basehttp . $_SERVER['SCRIPT_NAME']; ?>';
            var current_query = '<? echo str_replace($_SERVER['SCRIPT_NAME'], '', $_SERVER['REQUEST_URI']); ?>';
            $(document).ready(function () {
                $(".popMemcached").colorbox({width: "1150px", height: "620px", iframe: true});

                $('textarea.editor-enabled').each(function () {
                    $(this).ckeditor();
                });

                $(document).delegate('.popEdit', 'click', function () {
                    var id = $(this).attr('data');
                    var link = $(this).attr('href');

                    var windowHeight = window.innerHeight;
                    windowHeight = parseInt(windowHeight) - 10;
                    console.log(windowHeight);

                    parent.$.colorbox({
                        width: "1000px",
                        height: windowHeight + "px",
                        iframe: true,
                        href: link,
                        onClosed: function () {
                            $("#record" + id).load("<? echo $basehttp ?>/admin/existing_content_row.php?id=" + id);
                        }
                    });
                    return false;
                });

                $(document).delegate('.popEditPublish', 'click', function () {
                    var id = $(this).attr('data');
                    var link = $(this).attr('href');
                    $.colorbox({
                        width: "1000px",
                        height: "700px",
                        iframe: true,
                        href: link,
                        onClosed: function () {

                            $("#record" + id).load("<? echo $basehttp ?>/admin/publish_row.php?id=" + id);
                        }
                    });
                    return false;
                });

                $("#stats").load("<? echo $basehttp ?>/admin/encoder_stats.php");
                var refreshId = setInterval(function () {
                    $("#stats").load('<? echo $basehttp ?>/admin/encoder_stats.php?randval=' + Math.random());
                }, 3000);
            });

            function deleteRow(id) {
                var res = confirm('Are you sure to delete this item?');
                if (res === true) {
                    $("#record" + id).css('opacity', 0.4);
                    $.get('<? echo $basehttp ?>/admin/delete_content.php?id=' + id + '&site=<? echo (int) $_GET[site]; ?>', function () {
                        $("#record" + id).fadeOut('slow');
                    });
                }
            }
            function deleteImage(id) {
                $("#image" + id).css('opacity', 0.4);
                $.get('<? echo $basehttp ?>/admin/delete_image.php?id=' + id, function () {
                    $("#image" + id).fadeOut('slow');
                });
            }
            function unassignRow(id) {
                $("#record" + id).css('opacity', 0.4);
                $.get('<? echo $basehttp ?>/admin/unassign_content.php?id=' + id, function () {
                    $("#record" + id).fadeOut('slow');
                });
            }
            function publishRow(id) {
                $("#record" + id).css('opacity', 0.4);
                $.get('<? echo $basehttp ?>/admin/publish_content.php?id=' + id + '&site=<? echo (int) $_GET[site]; ?>', function () {
                    $("#record" + id).fadeOut('slow');
                });
            }
            function approveRow(id) {
                $("#record" + id).css('opacity', 0.4);
                $.get('<? echo $basehttp ?>/admin/approve_content.ajax.php?id=' + id + '&site=<? echo (int) $_GET[site]; ?>', function () {
                    $("#record" + id).fadeOut('slow');
                });
            }
            function swapThumb(id, url, newthumb) {
                document.getElementById('thumb' + id).src = url;
                $.get('<? echo $basehttp ?>/admin/swap_thumb.php?id=' + id + '&thumb=' + newthumb);
                $.colorbox.close()
            }
        </script>
    </head>
    <body>

        <div id="page" class="page-dashboard">
            
            <? include 'sidebar-left.php'; ?>

            <section id="content" class="page-content">

                <aside id="sidebar-right" class="sidebar-right">

                    <header id="header-user" class="page-header">
                        <div class="logged">
                            <span><i></i>Admin</span>
                            <a href="<? echo $basehttp; ?>/admin/logout.php" title="Logout" class="logout spr"></a>
                        </div>
                    </header>

                    <div id="block-thumbnailer-stats" class="block" data-ajax-refresh="<? echo $basehttp; ?>/admin/thumbnailer_stats.php" data-refresh-interval="5">
                        <? include_once "./thumbnailer_stats.php"; ?>
                    </div>

                    <div id="block-csv-photos-stats" class="block" data-ajax-refresh="<? echo $basehttp; ?>/admin/csv_photos_stats.php" data-refresh-interval="5">
                        <? include_once "./csv_photos_stats.php"; ?>
                    </div>

                    <div id="block-quick-stats" class="block">
                        <h2>Quick Stats</h2>
                        <div class="content">
                            <ul>
                                <li><a href='<? echo $basehttp; ?>/admin/existing_content.php'>Galleries Published<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `content` WHERE `approved` = 2 AND `enabled` = 1", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/publish.php'>Publish Queue<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `content` WHERE `approved` = 2 AND `enabled` = 0", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/queue.php'>Approval Queue<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `content` WHERE `approved` = 0", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/thumbnailer_queue.php'>Thumbnailing Queue<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM content WHERE (approved = 1 AND enabled = 0) OR rethumbnail = 1", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/csv_photos_queue.php'>CSV Import Queue<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `csv_import_photos`", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/models.php'>Models<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `models`", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/paysites.php'>Paysites<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `paysites`", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/niches.php'>Channels<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `niches`", 'count'); ?></span></a></li>  
                                <li><a href='<? echo $basehttp; ?>/admin/users.php'>Users<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `users` WHERE `user_level` < 2", 'count'); ?></span></a></li>
                                <li><a href='<? echo $basehttp; ?>/admin/users.php'>Partners<span><? echo dbValue("SELECT COUNT(1) AS `count` FROM `users` WHERE `user_level` = 2", 'count'); ?></span></a></li>
                            </ul>
                        </div>
                    </div>

                </aside> <!-- // #sidebar-right -->

                <div id="main-content" class="main-content">

                    <div class="content-area">