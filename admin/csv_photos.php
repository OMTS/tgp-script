<?
ignore_user_abort();
ini_set('max_execution_time', 86400);

require "db.php";

$active_menu = 'galleries';

if (isset($_POST['formSubmit'])) {
    requestTokenValidate($_POST['token'], 'csv-photos');

    $_POST = array_map_array('trim', $_POST);

    if ($_POST['submitter'] != "") {
        $user = dbRow("SELECT `record_num` FROM `users` WHERE `username` = '" . mysql_real_escape_string($_POST['submitter']) . "'");
        if (!is_array($user)) {
            setMessage('Selected user does not exist.', 'error');
        }
    }

    if ($_POST['dump'] == '') {
        setMessage('"Dump" cannot be empty!', 'error');
    }

    if (!getMessages(false, 'error')) {
        unset($arr);
        $arr = explode("\n", $_POST['dump']);
        foreach ($arr as $key => $value) {
            $k = explode('|', $value);
            for ($i = 1; $i < 8; $i++) {
                if ($_POST['select' . $i]) {
                    $import[$key][$_POST['select' . $i]] = $k[$i - 1];
                }
            }
        }

        $counter = 0;
        $table = 'csv_import_photos';
        if (is_array($import)) {
            foreach ($import as $i) {
                if ((!isset($i['flv']) || $i['flv'] == "") && ($i['plugurl'] != "")) {
                    $i['flv'] = $i['plugurl'];
                }
                if (!is_array(dbRow("SELECT `record_num` FROM `csv_import_photos` WHERE `flv` = '" . mysql_real_escape_string($i['flv']) . "'"))) {
                    $data = array(
                        'title' => $i['title'],
                        'flv' => strtr($i['flv'], array("\r" => "", "\n" => "")),
                        'thumb' => $i['thumb'],
                        'desc' => $i['desc'],
                        'embed' => $i['embed'],
                        'keywords' => $i['keywords'],
                        'lengthsec' => $i['length'],
                        'paysite' => $i['paysite'],
                        'default_paysite' => $_POST['default_paysite'],
                        'niche' => $_POST['default_niche'],
                        'hotlink' => $_POST['ishotlinked'],
                        'submitter' => is_array($user) ? $user['record_num'] : 0,
                        'models' => $i['models'],
                        'local' => $i['local'],
                        'plugurl' => $i['plugurl'],
                    );
                    dbInsert($table, $data, true);
                    $counter++;
                }
            }
        }

        setMessage("<strong>$counter galleries added to import queue.</strong>");
        if ($counter > 0) {
            shell_exec("pkill -f csv_photos_cron.php");
            dbQuery("UPDATE `status` SET `csv_import_running` = 0, `csv_import_total` = 0", false);
            $pid = backgroundProcess("$php_path $basepath/admin/csv_photos_cron.php", "csv_photo_log_thread1.txt");
            sleep(1);
            $pid = backgroundProcess("$php_path $basepath/admin/csv_photos_cron.php", "csv_photo_log_thread2.txt");
            sleep(1);
            $pid = backgroundProcess("$php_path $basepath/admin/csv_photos_cron.php", "csv_photo_log_thread3.txt");
            sleep(1);
            $pid = backgroundProcess("$php_path $basepath/admin/csv_photos_cron.php", "csv_photo_log_thread4.txt");
            sleep(1);
            $pid = backgroundProcess("$php_path $basepath/admin/csv_photos_cron.php", "csv_photo_log_thread5.txt");
            setMessage("CSV Importer has been started as PID: $pid");
        }
        header("Location: $_SERVER[REQUEST_URI]");
        exit();
    }
}

$paysites = dbQuery("SELECT `record_num`, `name` FROM `paysites` ORDER BY `name` ASC", false);
$niches = dbQuery("SELECT `record_num`, `name` FROM `niches` ORDER BY `name` ASC", false);

require "header.php";
?>

<header id="header" class="page-header">
    <div id="breadcrumbs">
        <i class="spr"></i>
        <ul>
            <li><a href="index.php">Admin Home</a></li>
            <li><a href="csv_photos.php">CSV Import</a></li>
        </ul>
    </div>

    <h1>CSV<span>Import</span></h1>

    <div class="page-hint">
        <p>Imports must be delimited by | (aka. pipes) only!</p>
        <p>Please note: This will not work on 100% of galleries! It will fail on photo galleries that do not link directly to jpg files.</p>
    </div>
</header>

<? echo getMessages(); ?>

<div class="content-inner">

    <form method="post" action="" class="form" autocomplete="off" novalidate>

        <div class="item">
            <label>Import Format</label>
            <div class="field csv-selects">
                <? for ($i = 1; $i < 8; $i++) { ?>
                    <select name="select<? echo $i; ?>" id="select<? echo $i + 1; ?>">
                        <option value="">-</option>
                        <option value="plugurl"<? echo $_REQUEST['select' . $i] == 'plugurl' ? ' selected' : ''; ?>>plug url</option>
                        <option value="flv"<? echo $_REQUEST['select' . $i] == 'flv' ? ' selected' : ''; ?>>url</option>
                        <option value="title"<? echo $_REQUEST['select' . $i] == 'title' ? ' selected' : ''; ?>>title</option>      
                        <option value="desc"<? echo $_REQUEST['select' . $i] == 'desc' ? ' selected' : ''; ?>>desc</option>
                        <option value="keywords"<? echo $_REQUEST['select' . $i] == 'keywords' ? ' selected' : ''; ?>>keywords</option>
                        <option value="paysite"<? echo $_REQUEST['select' . $i] == 'paysite' ? ' selected' : ''; ?>>paysite</option>
                        <option value="models"<? echo $_REQUEST['select' . $i] == 'models' ? ' selected' : ''; ?>>models</option>
                    </select>
                <? } ?>
            </div>
        </div>

        <div class="item">
            <label for="form-default_paysite">Default Paysite:</label>
            <div class="field">
                <select name="default_paysite" id="form-default_paysite">
                    <? if (is_array($paysites)) { ?>
                        <? foreach ($paysites as $paysite) { ?>
                            <option value="<? echo $paysite['record_num']; ?>"<? echo $_REQUEST['default_paysite'] == $paysite['record_num'] ? ' selected' : ''; ?>><? echo $paysite['name']; ?></option>
                        <? } ?>
                    <? } ?>
                </select>
            </div>
        </div>

        <div class="item">
            <label for="form-default_niche">Default Category:</label>
            <div class="field">
                <select name="default_niche" id="form-default_niche">
                    <? if (is_array($niches)) { ?>
                        <? foreach ($niches as $niche) { ?>
                            <option value="<? echo $niche['record_num']; ?>"<? echo $_REQUEST['default_niche'] == $niche['record_num'] ? ' selected' : ''; ?>><? echo $niche['name']; ?></option>
                        <? } ?>
                    <? } ?>
                </select>
            </div>
        </div>

        <div class="item">
            <label for="form-submitter">Submitter:</label>
            <? if ($conf['admin_user_field_type'] === 'autocomplete') { ?>
                <div class="field autocomplete-wrapper">
                    <input type="text" name="submitter" placeholder="start typing username..." data-autocomplete="1" data-url="<? echo $basehttp; ?>/admin/ajax/autocomplete.username.php" id="form-submitter" value="<? echo htmlentities($_POST['submitter'], ENT_QUOTES, 'UTF-8'); ?>">
                    <div class="autocomplete-container"></div>
                </div>
            <? } else { ?>
                <div class="field">
                    <select name="submitter" id="form-submitter">
                        <? $users = dbQuery("SELECT `record_num`, `username` FROM `users` WHERE `enabled` = 1 ORDER BY `username` ASC", false); ?>
                        <? if (is_array($users)) { ?>
                            <? foreach ($users as $user) { ?>
                                <option value="<? echo $user['username']; ?>"<? echo $user['record_num'] == $_POST['submitter'] ? ' selected' : ''; ?>><? echo $user['username']; ?></option>
                            <? } ?>
                        <? } ?>
                    </select>
                </div>
            <? } ?>
        </div>

        <div class="item">
            <label for="form-dump">Dump:</label>
            <div class="field">
                <textarea name="dump" rows="10" id="form-dump" wrap="off" required><? echo htmlentities($_REQUEST['dump'], ENT_QUOTES, 'UTF-8'); ?></textarea>
            </div>
        </div>

        <div class="item submit">
            <input type="hidden" name="formSubmit" value="1">
            <input type="hidden" name="token" value="<? echo requestTokenPrepare('csv-photos'); ?>">
            <button type="submit" class="btn">Import</button>
        </div>

    </form>

</div>
<? require "footer.php"; ?>
