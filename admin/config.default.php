<?

/* DATABASE INFORMATION */
$dbserver = '';
$dbname = '';
$dbuser = '';
$dbpass = '';
$dbprefix = '';

/* LICENSE KEY */
$licenseKey = '';

/* SIDE ID (MULTISITE WITH SHARED CONTENT AND USERS, DIFFERENT CONFIG/META) */
$site_id = 1;

/* ADMIN ACCESS */
$admin_username = 'admin'; //admin area username
$admin_password = 'admin'; //admin area password 
$admin_ip_limitation = false; //enabled admin area ip restrictions
$admin_allowed_ips = array(); //allowed ip address if above is enabled

/* SYSTEM PATHS INFORMATION */
$basepath = ''; //base absolutely linux path
$basehttp = 'http://yourdomain.com'; //base http path
$mediadomain = 'yourdomain.com/media'; //lighttpd install domain, usually media.domain.com

$language = 'en';

$temp_users_uploads = $basepath . '/temp_users_uploads'; //full unix path to upload dir, must be chmod 777
$thumb_path = $basepath . '/media/thumbs'; //full unix path for thumb storage
$misc_path = $basepath . '/media/misc'; //full unix path for misc path
$gallery_path = $basepath . '/media/galleries'; //gallery image path
$models_path = $basepath . '/media/models'; //models image path
$channels_path = $basepath . '/media/channels'; //channels image path
$paysites_path = $basepath . '/media/paysites'; //paysites image path
$avatars_path = $basepath . '/media/avatars'; //avatars image path
$banners_path = $basepath . '/media/banners'; //banners image path
$ftp_path = $basepath . '/ftp_photos'; //directory to ftp uploads
$csv_path = $basepath . '/csv_photos'; //directory to csv scraped photos
$cache_path = $basepath . '/cache'; //directory for cached files

$thumb_url = 'http://' . $mediadomain . '/thumbs'; //url to $thumb_path
$misc_url = 'http://' . $mediadomain . '/misc'; //url to $misc_path
$gallery_url = 'http://' . $mediadomain . '/galleries'; //url to $gallery_path
$models_url = 'http://' . $mediadomain . '/models'; //models image path
$channels_url = 'http://' . $mediadomain . '/channels'; //channels image path
$paysites_url = 'http://' . $mediadomain . '/paysites'; //paysites image path
$avatars_url = 'http://' . $mediadomain . '/avatars'; //avatars image path
$banners_url = 'http://' . $mediadomain . '/banners'; //banners image path

// REQUEST TOKEN
$requestTokenSalt = md5($_SERVER['HTTP_HOST'] . $licenseKey);

/* DEPENDANCIES */
$php_path = '/usr/local/bin/php'; //full unix path to php cli
$convert_path = '/usr/local/bin/convert'; //path to imagemagick's convert
$mogrify_path = '/usr/local/bin/mogrify'; //path to imagemagick's mogrify

/* SCRIPT SETTINGS */
$imagick_enabled = true; //enable or disable imagemagick processing on thumbs
$imagick_command = "-modulate 110,102,100 -sharpen 1x1 -enhance"; //command line for imagemagick

/* THUMBNAIL SIZES */
$picThumbSizes[] = array('width' => 104, 'height' => 170);
$picThumbSizes[] = array('width' => 170, 'height' => 104);
$picThumbSizes[] = array('width' => 117, 'height' => 117);
$picThumbSizes[] = array('width' => 199, 'height' => 259);
$picThumbSizes[] = array('width' => 236, 'height' => 355);
$picThumbSizes[] = array('width' => 295, 'height' => 297);
$picThumbSizes[] = array('width' => 590, 'height' => 594);
$picThumbSizes[] = array('width' => 664, 'height' => 468);
$picThumbSizes[] = array('width' => 1180, 'height' => 660);
$picThumbSizes[] = array('width' => 472, 'height' => 594);
$picThumbSizes[] = array('width' => 236, 'height' => 297);
$picThumbSizeFull = '1180x660';

/* MODELS THUMBNAIL SIZES */
$picModelsSizes[] = array('width' => 104, 'height' => 170);
$picModelsSizes[] = array('width' => 170, 'height' => 104);
$picModelsSizes[] = array('width' => 199, 'height' => 259);
$picModelsSizes[] = array('width' => 236, 'height' => 355);

/* CHANNELS THUMBNAIL SIZES */
$picChannelsSizes[] = array('width' => 104, 'height' => 170);
$picChannelsSizes[] = array('width' => 170, 'height' => 104);
$picChannelsSizes[] = array('width' => 199, 'height' => 259);
$picChannelsSizes[] = array('width' => 199, 'height' => 199);
$picChannelsSizes[] = array('width' => 236, 'height' => 355);

/* PAYSITES THUMBNAIL SIZES */
$picPaysitesSizes[] = array('width' => 104, 'height' => 170);
$picPaysitesSizes[] = array('width' => 170, 'height' => 104);
$picPaysitesSizes[] = array('width' => 199, 'height' => 259);
$picPaysitesSizes[] = array('width' => 236, 'height' => 355);

/* MEMBERS AVATARS THUMBNAIL SIZES */
$picAvatarsSizes[] = array('width' => 104, 'height' => 170);
$picAvatarsSizes[] = array('width' => 170, 'height' => 104);
$picAvatarsSizes[] = array('width' => 199, 'height' => 199);
$picAvatarsSizes[] = array('width' => 236, 'height' => 177);
$picAvatarsSizes[] = array('width' => 65, 'height' => 65);

$enableMobile = false; //enable mobile template, otherwise mobile will use regular template.
$redirect_mobile = false; //set to url of mobile version, otherwise set $redirect_mobile = false; $enableMobile must be set to false.

$email_notifications_triggers = array(
    'new-message' => 'new_message',
    'new-post' => 'new_post',
    'new-comment' => 'new_comment',
    'friend-request' => 'friend_request',
);

$custom_user_fields = array();

$custom_model_fields = array(
    "City" => "",	// simple textfield
    "Sexual Orientation" => array("Straight", "Lesbian", "Gay", "Bi-Sexual"),	// select (dropdown)
    "Facebook" => array(
        "type" => "html",
    ),	// textfield (by default) with HTML tags allowed
    "Twitter" => array(
        "type" => "html",
    ),	// textfield (by default) with HTML tags allowed
    "Instagram" => array(
        "type" => "html",
    ),	// textfield (by default) with HTML tags allowed
    "About Me" => array(
        "field" => "textarea",
        "type" => "html",
        "editor" => true,
    ),	// textarea with HTML tags allowed and WYSIWYG editor attached
);