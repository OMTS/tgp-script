<?
require "db.php";

$variable_groups = array(
    'system' => 'Basic System Settings',
    'system_status' => 'Performance &amp; Development Settings',
    'email' => 'E-mail Settings',
    'gallery' => 'Gallery Settings',
    'user' => 'Member Settings',
    'model' => 'Model Settings',
    'signup' => 'Signup Settings',
    'content' => 'Content Settings',
    'cron' => 'Cron Settings',
    'other' => 'Additional Settings',
);

$variables = array();
$results = dbQuery("SELECT * FROM `variables` WHERE `scope` = 'config' AND `site_id` = '" . (int) $site_id . "' ORDER BY `record_num` ASC", false);
foreach ($results as $var) {
    $var['value'] = ($var['value'] != "") ? unserialize($var['value']) : '';
    $var['options'] = ($var['options'] != "") ? unserialize($var['options']) : '';
    $var['info'] = ($var['info'] != "") ? unserialize($var['info']) : '';
    if (in_array($var['value'], array(0, 1)) && empty($var['options'])) {
        $var['options'] = array(1 => 'Yes', 0 => 'No');
    }
    $variables[$var['group']][$var['variable']] = $var;
}

if (isset($_GET['rebuild'])) {
    rebuildThemeFiles();
    rebuildThemeFiles(true);
    header("Location: $basehttp/admin/system_config.php");
    exit;
}

if (isset($_POST['formSubmit'])) {
    requestTokenValidate($_POST['token'], 'system-config');

    /* Validate caching system (memcache/filesystem) */
    if(isset($_POST['cache_type'])) {
        if($_POST['cache_type'] === 'memcached') {
            if(extension_loaded('memcache') === false) {
                setMessage('Memcache is not installed!', 'error');
            } else {
                $memcache = new Memcache();
                if($memcache->connect($_POST['cache_memcached_server'], $_POST['cache_memcached_port']) === false) {
                    setMessage('Cannot connect to memcache (hostname/IP and/or port are not correct).', 'error');
                }
            }
        } else if($_POST['cache_type'] === 'filesystem') {
            @mkdir($cache_path, 0777);
            @chmod($cache_path, 0777);
            if(!is_writable($cache_path)) {
                setMessage("<em>$cache_path</em> is not writable!", 'error');
            }
        }
    }
    
    if (!getMessages(false, 'error')) {
        unset($_POST['formSubmit'], $_POST['token']);
        foreach ($_POST as $key => $value) {
            setVariable($key, $value);
        }
        setMessage('TGP Configuration has been saved.');
        header("Location: $basehttp/admin/system_config.php?rebuild");
        exit;
    }
}

$active_menu = 'server-status';
?>

<? include "header.php"; ?>

<header id="header" class="page-header">

    <div id="breadcrumbs">
        <i class="spr"></i>
        <ul>
            <li><a href="index.php">Admin Home</a></li>
            <li><a href="system_config.php">TGP Configuration</a></li>
        </ul>
    </div>

    <h1>TGP<span>Configuration</span></h1>

</header>

<? echo getMessages(); ?>

<div class="content-inner">

    <form action="" method="post" class="form form-configuration" enctype="multipart/form-data" autocomplete="off" novalidate>

        <? foreach ($variable_groups as $group_key => $group_name) { ?>
            <? if (isset($variables[$group_key])) { ?>
                <h3 class="head small"><? echo $group_name; ?></h3>            
                <div class="contents">
                    <? foreach ($variables[$group_key] as $variable => $data) { ?>
                        <? if ($data['type'] == 'select' || $data['type'] == 'multiselect') { ?>
                            <div class="item">
                                <label for="form-<? echo $variable; ?>"><? echo $data['name']; ?>:</label>
                                <div class="field">
                                    <select name="<? echo $variable; ?>" id="form-<? echo $variable; ?>"<? echo ($data['type'] == 'multiselect') ? ' multiple' : ''; ?>>
                                        <option value="">- select -</option>
                                        <? foreach ($data['options'] as $key => $val) { ?>
                                            <? if (is_array($val)) { ?>
                                                <optgroup label="<? echo ucfirst($key); ?>">
                                                    <? foreach ($val as $_key => $_val) { ?>
                                                        <option value="<? echo $_key; ?>"<? echo $data['value'] == "$_key" ? ' selected="selected"' : ''; ?>><? echo $_val; ?></option>
                                                    <? } ?>
                                                <? } else { ?>
                                                    <option value="<? echo $key; ?>"<? echo $data['value'] == "$key" ? ' selected="selected"' : ''; ?>><? echo $val; ?></option>
                                                <? } ?>
                                            <? } ?>
                                    </select>
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } elseif ($data['type'] == 'checkbox') { ?>
                            <div class="item">
                                <div class="field">
                                    <? if (is_array($data['options'])) { ?>
                                        <? foreach ($data['options'] as $key => $val) { ?>
                                            <label for="form-<? echo $variable; ?>-<? echo $key; ?>" class="checkbox">
                                                <input type="checkbox" name="<? echo $variable; ?>[<? echo $key; ?>]" value="<? echo $key; ?>" id="form-<? echo $variable; ?>-<? echo $key; ?>"<? echo array_key_exists($key, $data['value']) ? ' checked="checked"' : ''; ?>>
                                                <i></i>
                                                <? echo $val; ?>:
                                            </label>
                                        <? } ?>
                                    <? } else { ?>
                                        <label for="form-<? echo $variable; ?>" class="checkbox">
                                            <input type="checkbox" name="<? echo $variable; ?>" value="<? echo $data['value']; ?>" id="form-<? echo $variable; ?>"<? echo ($data['value'] == 1) ? ' checked="checked"' : ''; ?>>
                                            <i></i>
                                            <? echo $data['name']; ?>:
                                        </label>
                                    <? } ?>
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } elseif ($data['type'] == 'radio') { ?>
                            <div class="item">
                                <div class="field">
                                    <? if (is_array($data['options'])) { ?>
                                        <? foreach ($data['options'] as $key => $val) { ?>
                                            <label for="form-<? echo $variable; ?>-<? echo $key; ?>" class="radio">
                                                <input type="radio" name="<? echo $variable; ?>" value="<? echo $key; ?>" id="form-<? echo $variable; ?>-<? echo $key; ?>"<? echo array_key_exists($key, $data['value']) ? ' checked="checked"' : ''; ?>>
                                                <i></i>
                                                <? echo $val; ?>:
                                            </label>
                                        <? } ?>
                                    <? } else { ?>
                                        <label for="form-<? echo $variable; ?>" class="radio">
                                            <input type="radio" name="<? echo $variable; ?>" value="<? echo $data['value']; ?>" id="form-<? echo $variable; ?>"<? echo ($data['value'] == 1) ? ' checked="checked"' : ''; ?>>
                                            <i></i>
                                            <? echo $data['name']; ?>:
                                        </label>
                                    <? } ?>
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } elseif ($data['type'] == 'number') { ?>
                            <div class="item">
                                <label for="form-<? echo $variable; ?>"><? echo $data['name']; ?>:</label>
                                <div class="field">
                                    <input type="number" name="<? echo $variable; ?>" value="<? echo htmlentities($data['value'], ENT_QUOTES, 'UTF-8'); ?>" id="form-<? echo $variable; ?>" <? echo isset($data['info']['min']) ? ' min="' . $data['info']['min'] . '"' : ''; ?><? echo isset($data['info']['max']) ? ' max="' . $data['info']['max'] . '"' : ''; ?><? echo isset($data['info']['step']) ? ' step="' . $data['info']['step'] . '"' : ''; ?>>
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } elseif ($data['type'] == 'textarea') { ?>
                            <div class="item">
                                <label for="form-<? echo $variable; ?>"><? echo $data['name']; ?>:</label>
                                <div class="field">
                                    <textarea name="<? echo $variable; ?>" id="form-<? echo $variable; ?>"><? echo htmlentities($data['value'], ENT_QUOTES, 'UTF-8'); ?></textarea>
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } elseif ($data['type'] == 'email') { ?>
                            <div class="item">
                                <label for="form-<? echo $variable; ?>"><? echo $data['name']; ?>:</label>
                                <div class="field">
                                    <input type="email" name="<? echo $variable; ?>" value="<? echo htmlentities($data['value'], ENT_QUOTES, 'UTF-8'); ?>" id="form-<? echo $variable; ?>">
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } elseif ($data['type'] == 'date') { ?>
                            <div class="item">
                                <label for="form-<? echo $variable; ?>"><? echo $data['name']; ?>:</label>
                                <div class="field">
                                    <input type="date" name="<? echo $variable; ?>" value="<? echo htmlentities($data['value'], ENT_QUOTES, 'UTF-8'); ?>" id="form-<? echo $variable; ?>"<? echo isset($data['info']['min']) ? ' min="' . $data['info']['min'] . '"' : ''; ?><? echo isset($data['info']['max']) ? ' max="' . $data['info']['max'] . '"' : ''; ?>>
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } else { ?>
                            <div class="item">
                                <label for="form-<? echo $variable; ?>"><? echo $data['name']; ?>:</label>
                                <div class="field">
                                    <input type="text" name="<? echo $variable; ?>" value="<? echo htmlentities($data['value'], ENT_QUOTES, 'UTF-8'); ?>" id="form-<? echo $variable; ?>">
                                    <? if (isset($data['info']['suffix']) && ($data['info']['suffix'] != "")) { ?>
                                        <span class="suffix"><? echo $data['info']['suffix']; ?></span>
                                    <? } ?>
                                </div>
                                <? if (isset($data['info']['hint']) && ($data['info']['hint'] != "")) { ?>
                                    <p class="hint"><? echo $data['info']['hint']; ?></p>
                                <? } ?>
                            </div>
                        <? } ?> 
                    <? } ?> 
                </div>
            <? } ?>
        <? } ?>

        <div class="item submit">
            <input type="hidden" name="formSubmit" value="1">
            <input type="hidden" name="token" value="<? echo requestTokenPrepare('system-config'); ?>">
            <button type="submit" class="btn">Save Settings</button>
        </div>

    </form>

</div> <!-- // .content-inner -->

<? require "footer.php"; ?>
