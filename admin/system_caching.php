<?
require "db.php";

if (!isset($conf['outdated_files_diff'])) {
    $conf['outdated_files_diff'] = 30 * 86400; // 30 days
}

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'setup_multisite':
            if (is_numeric($site_id) && $site_id > 1) {
                $_metatags = dbQuery("SELECT * FROM `metatags` WHERE `site_id` = 1", false);
                foreach ($_metatags as $item) {
                    unset($item['record_num']);
                    if (isset($site_id)) {
                        $item['site_id'] = $site_id;
                    }
                    dbInsert('metatags', $item, true);
                }
                $_variables = dbQuery("SELECT * FROM `variables` WHERE `site_id` = 1", false);
                foreach ($_variables as $item) {
                    unset($item['record_num']);
                    if (isset($site_id)) {
                        $item['site_id'] = $site_id;
                    }
                    dbInsert('variables', $item, true);
                }
            }
            break;
        case 'rebuild_css_js':
            rebuildThemeFiles();
            rebuildThemeFiles(true);
            break;
        case 'cleanup':
            cleanupThemeFiles();
            break;
        case 'clear_caches':
            if ($cache_path == "" || strpos($_SERVER['DOCUMENT_ROOT'], $cache_path)) {
                setMessage("Seems like <em>$" . "cache_path</em> is not configured correctly. Make sure $cache_path located inside $_SERVER[DOCUMENT_ROOT]", 'error');
            } else {
                setMessage("Caches in $cache_path/ cleared");
                shell_exec("rm -rf $cache_path/*");
                dbQuery("UPDATE `variables` SET `options` = '" . serialize(getThemesList()) . "' WHERE `variable` = 'template_name' AND `site_id` = '" . (int) $site_id . "'", false);
            }
            break;
    }
    header("Location: system_caching.php");
    exit;
}

$cleanup_information = cleanupThemeFiles(true);

$active_menu = 'server-status';
?>

<? require "header.php"; ?>

<header id="header" class="page-header">

    <div id="breadcrumbs">
        <i class="spr"></i>
        <ul>
            <li><a href="index.php">Admin Home</a></li>
            <li><a href="system_caching.php">System Caching</a></li>
        </ul>
    </div>

    <h1>System<span>Caching</span></h1>

</header>

<? echo getMessages(); ?>

<div class="content-inner">

    <div class="form-table">
        <table>
            <thead>
                <tr>
                    <th>Action</th>
                    <th>Info</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href="<? echo $basehttp; ?>/admin/system_caching.php?action=rebuild_css_js" class="btn red">Rebuild CSS/JS Files</a></td>
                    <td>Last rebuilt on: <b><? echo dbValue("SELECT `date_updated` FROM `variables` WHERE `variable` = 'filehash'", 'date_updated'); ?></b></td>
                </tr>
                <tr>
                    <td><a href="<? echo $basehttp; ?>/admin/system_caching.php?action=cleanup" class="btn red">Cleanup Old CSS/JS Files</a></td>
                    <td><b><? echo (int) $cleanup_information['amount_outdated']; ?></b> outdated files found (older than <? echo floor(($conf['outdated_files_diff'] * 3600) / 86400); ?> days).</td>
                </tr>
                <tr>
                    <td><a href="<? echo $basehttp; ?>/admin/system_caching.php?action=clear_caches" class="btn red">Clear Caches</a></td>
                    <td><em>Click to clear cached filesystem caches and refresh system variables.</em></td>
                </tr>
                <? if (is_numeric($site_id) && $site_id > 1) { ?>
                    <tr>
                        <td><a href="<? echo $basehttp; ?>/admin/system_caching.php?action=setup_multisite" class="btn red">Setup Multisite Instance</a></td>
                        <td><em>Click to setup multisite configuration for this instance.</em></td>
                    </tr>
                <? } ?>
            </tbody>
        </table>
    </div>

</div> <!-- // .content-inner -->

<? require "footer.php"; ?>
