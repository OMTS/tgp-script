<?

function gcd($a, $b) {
    if ($a == 0 || $b == 0)
        return abs(max(abs($a), abs($b)));

    $r = $a % $b;
    return ($r != 0) ? gcd($b, $r) : abs($b);
}

function editPhotoWithCrop($params, $type) {
    global $gallery_path, $thumb_path, $conf, $convert_path, $mogrify_path, $imagick_command;
    global $channels_path, $models_path, $avatars_path, $paysites_path;

    $params['preset'] = $params['image_size'];
    unset($params['image_size']);

    switch ($type) {
        case 'channel':
            $photo = dbRow("SELECT * FROM `niches` WHERE `record_num` = '" . $params['photoid'] . "'");
            $thumbpath = $filepath = $channels_path;
            break;
        case 'model':
            $photo = dbRow("SELECT * FROM `models` WHERE `record_num` = '" . $params['photoid'] . "'");
            $thumbpath = $filepath = $models_path;
            break;
        case 'avatar':
            $photo = dbRow("SELECT * FROM `users` WHERE `record_num` = '" . $params['photoid'] . "'");
            $thumbpath = $filepath = $avatars_path;
            break;
        case 'paysite':
            $photo = dbRow("SELECT * FROM `paysites` WHERE `record_num` = '" . $params['photoid'] . "'");
            $thumbpath = $filepath = $paysites_path;
            break;
        case 'content':
        default:
            $photo = dbRow("SELECT `images`.`filename`, `images`.`crop_settings`, (SELECT `directory` FROM `content` WHERE `record_num` = `images`.`gallery`) AS `directory` FROM `images` WHERE `images`.`record_num` = '" . $params['photoid'] . "'");
            $filepath = $gallery_path;
            $thumbpath = $thumb_path;
            break;
    }

    if (is_array($photo)) {
        $photoPath = $photo['directory'][0] . '/' . $photo['directory'][1] . '/' . $photo['directory'][2] . '/' . $photo['directory'][3] . '/' . $photo['directory'][4] . '/' . $photo['directory'];

        $filename = $photo['filename'];
        $extension = explode(".", $filename);
        $ext = strtolower($extension[1]);
        $input = $filepath . '/' . $photoPath . '/' . $filename;
        $output = $thumbpath . "/" . $photoPath . "/" . $params['preset'] . "/" . $filename;

        if (!is_dir($thumbpath . "/" . $photoPath . "/" . $params['preset'])) {
            @mkdir($thumbpath . "/" . $photoPath . "/" . $params['preset']);
            chmod($thumbpath . "/" . $photoPath . "/" . $params['preset'], 0777);
        }

        if (!is_dir($thumbpath . "/" . $photoPath . "/temp")) {
            @mkdir($thumbpath . "/" . $photoPath . "/temp");
            chmod($thumbpath . "/" . $photoPath . "/temp", 0777);
        }

        $params['preview_width'] = (int) $params['preview_width'];
        $params['preview_height'] = (int) $params['preview_height'];
        $params['preview_x'] = (int) $params['preview_x'];
        $params['preview_y'] = (int) $params['preview_y'];

        $preset = explode("x", $params['preset']);
        if ($params['preview_width'] != 0 && $params['preview_height'] != 0) {
            
            $target_width = $preset[0];
            $target_height = $preset[1];
			
			shell_exec("rm -f $output");
            
            $image_quality = (isset($conf['image_quality']) && $conf['image_quality'] > 0) ? (int) $conf['image_quality'] : 85;

            switch($ext) {
                case 'png':
                    $src_img = imagecreatefrompng($input);
                    $dst_img = imagecreatetruecolor($target_width, $target_height);
                    imagecopyresampled($dst_img, $src_img, 0, 0, $params['preview_x'], $params['preview_y'], $target_width, $target_height, $params['preview_width'], $params['preview_height']);
                    imagepng($dst_img, $output, 9);
                    break;
                case 'gif':
                    $src_img = imagecreatefromgif($input);
                    $dst_img = imagecreatetruecolor($target_width, $target_height);
                    imagecopyresampled($dst_img, $src_img, 0, 0, $params['preview_x'], $params['preview_y'], $target_width, $target_height, $params['preview_width'], $params['preview_height']);
                    imagegif($dst_img, $output);
                    break;
                default:
                    $src_img = imagecreatefromjpeg($input);
                    $dst_img = imagecreatetruecolor($target_width, $target_height);
                    imagecopyresampled($dst_img, $src_img, 0, 0, $params['preview_x'], $params['preview_y'], $target_width, $target_height, $params['preview_width'], $params['preview_height']);
                    imagejpeg($dst_img, $output, $image_quality);
                    break;
            }
            
            shell_exec("$convert_path \"$output\" -quality $image_quality $imagick_command \"$output.tmp\"");
            shell_exec("rm \"$output\"");
            shell_exec("mv \"$output.tmp\" \"$output\"");
			
			chmod($output, 0666);

            $photo['crop'] = (!empty($photo['crop_settings'])) ? unserialize($photo['crop_settings']) : false;

            if ($photo['crop'] !== false) {
                if (is_array($photo['crop'][$params['preset']])) {
                    unset($photo['crop'][$params['preset']]);
                }
            }

            $photo['crop'][$params['preset']] = array(
                'preview_width' => $params['preview_width'],
                'preview_height' => $params['preview_height'],
                'preview_x' => $params['preview_x'],
                'preview_y' => $params['preview_y']
            );

            $crop_settings = serialize($photo['crop']);
            switch ($type) {
                case 'channel':
                    dbUpdate('niches', array('crop_settings' => $crop_settings, 'record_num' => $params['photoid']));
                    break;
                case 'model':
                    dbUpdate('models', array('crop_settings' => $crop_settings, 'record_num' => $params['photoid']));
                    break;
                case 'avatar':
                    dbUpdate('users', array('crop_settings' => $crop_settings, 'record_num' => $params['photoid']));
                    break;
                case 'paysite':
                    dbUpdate('paysites', array('crop_settings' => $crop_settings, 'record_num' => $params['photoid']));
                    break;
                case 'content':
                default:
                    dbUpdate('images', array('crop_settings' => $crop_settings, 'record_num' => $params['photoid']));
                    break;
            }
        } else {
			shell_exec("rm -f $output");
            makeImageThumbnail($input, $output, $preset[0], $preset[1]);
			chmod($output, 0666);
        }

        return true;
    } else {
        return false;
    }
}
