<?php

/* THIS FILE CONTAINS GENERAL FUNCTIONS WHERE ARE VERY INFREQUENTLY MODIFIED. PLEASE AVOID MODIFYING THIS FILE. */

//check if device is mobile
if ($enableMobile) {
    if (detectMobile()) {
        $isMobile = true;
    }
}

//redirect mobile phones away from site
if ($redirect_mobile) {
    if (detectMobile()) {
        header("Location: $redirect_mobile");
        exit();
    }
}

function updateRelatedContent($id) {
    $row = dbRow("SELECT * FROM `content` WHERE `record_num` = '$id'");
    if (is_array($row)) {
        $relatedContent = array();
        $string = mysql_real_escape_string("$row[title] $row[keywords]");
        $results = dbQuery("SELECT `content`.*, (MATCH (`title`, `keywords`, `description`) AGAINST ('$string')) AS `score` FROM `content` WHERE `enabled` = 1 AND `record_num` != '" . (int) $row['record_num'] . "' AND MATCH (`title`, `keywords`, `description`) AGAINST ('$string' IN BOOLEAN MODE) HAVING `score` > 0 ORDER BY `score` DESC LIMIT 0, 50", false);
        if (is_array($results)) {
            foreach ($results as $rrow) {
                $relatedContent[] = $rrow['record_num'];
            }
        }
        dbUpdate('content', array('related' => implode(',', $relatedContent), 'record_num' => $row['record_num']));
    }
}

function generateUrl($type, $name, $content_id) {
    global $basehttp;

    switch ($type) {
        case 'galleries':
            return $basehttp . '/' . $type . '/' . clearString($name) . '-' . $content_id . '.html';
            break;
        case 'channel':
            return $basehttp . '/channels/' . $content_id . '/' . clearString($name) . '/';
            break;
        case 'paysite':
            return $basehttp . '/paysites/' . $content_id . '/' . clearString($name) . '/';
            break;
        case 'model':
            return $basehttp . '/models/' . clearString($name) . '-' . $content_id . '.html';
            break;
        case 'user':
            return $basehttp . '/' . $type . '/' . clearString($name) . '-' . $content_id . '/';
            break;
        default:
            return $basehttp . '/' . $type . '/' . clearString($name) . '-' . $content_id . '.html';
            break;
    }
}

function validateUser($username, $password) {
    $validate = dbQuery("SELECT * FROM users WHERE username = '$username' AND password = MD5(CONCAT('$password',users.salt))", false);

    if (is_array($validate)) {
        return $validate;
    } else {
        return false;
    }
}

//detects mobile devices. You can edit the string that matches them here
function detectMobile() {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    if ($_GET['mobileDevice']) {
        return true;
    }
    switch (true) {
        case (stripos($user_agent, 'android'));
            $isMobile = true;
            break;
        case (stripos($user_agent, 'iphone') || stripos($user_agent, 'ipod'));
            $isMobile = true;
            break;
        case (stripos($user_agent, 'opera mini'));
            $isMobile = true;
            break;
        case (stripos($user_agent, 'blackberry'));
            $isMobile = true;
            break;
        case (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo|palm os|palm|hiptop|avantgo|fennec|plucker|xiino|blazer|elaine|iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile)/i', $user_agent));
            $isMobile = true;
            break;
    }
    return $isMobile;
}

function isAjax() {
    return strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
}

function prepareFilePath($path, $dir) {
    if (!isset($path) || $path == "" || !isset($dir) || $dir == "") {
        setMessage('Path or directory not found in <em>' . __FUNCTION__ . '()</em> arguments.', 'error');
        return;
    }

    $first = $dir[0];
    $second = $dir[1];
    $third = $dir[2];
    $forth = $dir[3];
    $fifth = $dir[4];

    @mkdir("$path");
    chmod("$path", 0777);
    @mkdir("$path/$first");
    chmod("$path/$first", 0777);
    @mkdir("$path/$first/$second");
    chmod("$path/$first/$second", 0777);
    @mkdir("$path/$first/$second/$third");
    chmod("$path/$first/$second/$third", 0777);
    @mkdir("$path/$first/$second/$third/$forth");
    chmod("$path/$first/$second/$third/$forth", 0777);
    @mkdir("$path/$first/$second/$third/$forth/$fifth");
    chmod("$path/$first/$second/$third/$forth/$fifth", 0777);
    @mkdir("$path/$first/$second/$third/$forth/$fifth/$dir");
    chmod("$path/$first/$second/$third/$forth/$fifth/$dir", 0777);

    return "$path/$first/$second/$third/$forth/$fifth/$dir";
}

function makeImageThumbnail($input, $output, $thumbwidth, $thumbheight, $mode = 'convert', $args = array()) {
    global $convert_path, $mogrify_path, $conf, $imagick_command;

    $image_quality = (isset($conf['image_quality']) && $conf['image_quality'] > 0) ? (int) $conf['image_quality'] : 85;

    if ($input == $output) {
        exit("Error: makeImageThumbnail() input and output cannot be the same!");
    }
    if ($mode == 'convert') {
        $convert_mode = $convert_path;
    } else {
        $convert_mode = $mogrify_path;
    }
    if ($args['fill_color'] != "") {
        shell_exec("$convert_mode -extent " . ($thumbwidth) . "x" . ($thumbheight) . " -gravity Center -fill $args[fill_color] -background $args[fill_color] \"$input\" -quality $image_quality $imagick_command \"$output\"");
    } else {
        shell_exec("$convert_mode -adaptive-resize " . ($thumbwidth) . "x" . ($thumbheight) . "^ -gravity Center -extent " . $thumbwidth . "x" . $thumbheight . " \"$input\" -quality $image_quality $imagick_command \"$output\"");
    }
}

function clearString($str) {
    $chars = array(
        chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
        chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
        chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
        chr(195) . chr(134) => 'AE', chr(195) . chr(166) => 'ae',
        chr(195) . chr(135) => 'C', chr(195) . chr(136) => 'E',
        chr(195) . chr(137) => 'E', chr(195) . chr(138) => 'E',
        chr(195) . chr(139) => 'E', chr(195) . chr(140) => 'I',
        chr(195) . chr(141) => 'I', chr(195) . chr(142) => 'I',
        chr(195) . chr(143) => 'I', chr(195) . chr(145) => 'N',
        chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
        chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
        chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
        chr(195) . chr(152) => 'O', chr(195) . chr(184) => 'o',
        chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
        chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
        chr(195) . chr(159) => 's', chr(195) . chr(160) => 'a',
        chr(195) . chr(161) => 'a', chr(195) . chr(162) => 'a',
        chr(195) . chr(163) => 'a', chr(195) . chr(164) => 'a',
        chr(195) . chr(165) => 'a', chr(195) . chr(167) => 'c',
        chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
        chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
        chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
        chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
        chr(195) . chr(177) => 'n', chr(195) . chr(178) => 'o',
        chr(195) . chr(179) => 'o', chr(195) . chr(180) => 'o',
        chr(195) . chr(181) => 'o', chr(195) . chr(182) => 'o',
        chr(195) . chr(182) => 'o', chr(195) . chr(185) => 'u',
        chr(195) . chr(186) => 'u', chr(195) . chr(187) => 'u',
        chr(195) . chr(188) => 'u', chr(195) . chr(189) => 'y',
        chr(195) . chr(191) => 'y',
        chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
        chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
        chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
        chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
        chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
        chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
        chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
        chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
        chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
        chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
        chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
        chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
        chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
        chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
        chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
        chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
        chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
        chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
        chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
        chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
        chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
        chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
        chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
        chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
        chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
        chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
        chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
        chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
        chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
        chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
        chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
        chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
        chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
        chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
        chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
        chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
        chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
        chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
        chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
        chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
        chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
        chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
        chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
        chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
        chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
        chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
        chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
        chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
        chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
        chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
        chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
        chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
        chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
        chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
        chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
        chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
        chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
        chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
        chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
        chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
        chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
        chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
        chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
        chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
        // Euro
        chr(226) . chr(130) . chr(172) => 'E',
        // Funt
        chr(194) . chr(163) => '');
    $str = strip_tags(strtolower(strtr($str, $chars)));
    $trans = array(
        '_' => '-',
        '\s+' => '-',
        '[^a-z0-9-\.]' => '',
        '-+' => '-'
    );
    foreach ($trans as $search => $replace) {
        $str = preg_replace("#" . $search . "#i", $replace, $str);
    }

    return trim($str, '-');
}

function pageDenied() {
    header('HTTP/1.0 403 Forbidden');
    $meta_settings = dbRow("SELECT * FROM `metatags` WHERE `controller` = '403'");
    $title = $meta_settings['meta_title'];
    $headertitle = $meta_settings['header_title'];
    setMessage('You have no rights to view this page.', 'error');
    getTemplate("template.overall_header.php");
    getTemplate('template.403.php');
    getTemplate("template.overall_footer.php");
    exit();
}

function pageNotFound() {
    header("HTTP/1.0 404 Not Found");
    $meta_settings = dbRow("SELECT * FROM `metatags` WHERE `controller` = '404'");
    $title = $meta_settings['meta_title'];
    $headertitle = $meta_settings['header_title'];
    setMessage('The requested file was not found on this server.', 'error');
    getTemplate('template.overall_header.php');
    getTemplate('template.404.php');
    getTemplate('template.overall_footer.php');
    exit();
}

function pageError() {
    global $basepath, $thisController, $title;
    if (!getMessages(false, 'error') && !getMessages(false, 'info')) {
        setMessage('Unexpected error occured', 'error');
    }
    $thisController = 'maintenance';
    include_once "$basepath/includes/inc.metatags.php";
    $title = 'Site is under maintenance';
    getTemplate('template.maintenance.php');
    exit();
}

function getMemoryUsage($decimals = 2) {
    $result = 0;
    if (function_exists('memory_get_usage')) {
        $result = memory_get_usage() / 1024;
    } else {
        if (function_exists('exec')) {
            $output = array();
            if (substr(strtoupper(PHP_OS), 0, 3) == 'WIN') {
                exec('tasklist /FI "PID eq ' . getmypid() . '" /FO LIST', $output);
                $result = preg_replace('/[\D]/', '', $output[5]);
            } else {
                exec('ps -eo%mem,rss,pid | grep ' . getmypid(), $output);
                $output = explode('  ', $output[0]);
                $result = $output[1];
            }
        }
    }
    return number_format(intval($result) / 1024, $decimals, '.', '');
}

function getFileList($directory, $extensions = array()) {
    if (is_dir($directory)) {
        $files = array();
        $handle = opendir($directory);
        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                if (is_dir("$directory/$file")) {
                    $files = array_merge($files, getFileList("$directory/$file", $extensions));
                } else {
                    $pathinfo = pathinfo("$directory/$file");
                    if (count($extensions) > 0) {
                        if (in_array($pathinfo['extension'], $extensions)) {
                            $files[] = "$directory/$file";
                        }
                    } else {
                        $files[] = "$directory/$file";
                    }
                }
            }
        }
        closedir($handle);

        return $files;
    }
}

if (!function_exists('formatBytes')) {

    function formatBytes($size, $precision = 2) {
        $base = log($size, 1024);
        $suffixes = array('B', 'kB', 'MB', 'GB', 'TB');

        return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
    }

}

/**
 * jsonpp - Pretty print JSON data
 *
 * In versions of PHP < 5.4.x, the json_encode() function does not yet provide a
 * pretty-print option. In lieu of forgoing the feature, an additional call can
 * be made to this function, passing in JSON text, and (optionally) a string to
 * be used for indentation.
 *
 * @param string $json  The JSON data, pre-encoded
 * @param string $istr  The indentation string
 *
 * @return string
 */
function jsonpp($json, $istr = '  ') {
    $result = '';
    for ($p = $q = $i = 0; isset($json[$p]); $p++) {
        $json[$p] == '"' && ($p > 0 ? $json[$p - 1] : '') != '\\' && $q = !$q;
        if (strchr('}]', $json[$p]) && !$q && $i--) {
            strchr('{[', $json[$p - 1]) || $result .= "\n" . str_repeat($istr, $i);
        }
        $result .= $json[$p];
        if (strchr(',{[', $json[$p]) && !$q) {
            $i += strchr('{[', $json[$p]) === FALSE ? 0 : 1;
            strchr('}]', $json[$p + 1]) || $result .= "\n" . str_repeat($istr, $i);
        }
    }
    return $result;
}

function rebuildThemeFiles($refreshMobile = false) {
    global $enableMobile, $conf;

    if ($refreshMobile === true && $enableMobile === false) {
        return false;
    }

    $parsedFiles = readTemplateInfo(false, $refreshMobile);

    setVariable('filehash' . ($refreshMobile ? '_m' : '') . '_' . $conf['template_name'], uniqid('', true));

    aggregateFiles($parsedFiles['css'], 'css', 'css', $refreshMobile);
    aggregateFiles($parsedFiles['css_print'], 'css', 'print', $refreshMobile);
    aggregateFiles($parsedFiles['js'], 'js', 'js', $refreshMobile);

    setMessage(($refreshMobile === true ? 'Mobile ' : '') . 'CSS and JS cache updated.', 'success');

    return true;
}

function readTemplateInfo($returnUrls = false, $readMobile = false) {
    global $template_path, $template_url, $basepath, $basehttp, $isMobile, $enableMobile;

    if (!file_exists("$template_path/template.info")) {
        return;
    }

    $files = array(
        'css' => array(),
        'css_print' => array(),
        'js' => array(),
    );

    $parsedInfo = parseInfoFile("$template_path/template.info");

    if (($enableMobile && $isMobile) || $readMobile === true) {
        if (is_array($parsedInfo['css_mobile'])) {
            foreach ($parsedInfo['css_mobile'] as $file) {
                switch (key($file)) {
                    case 'theme':
                        $files['css'][] = ($returnUrls === true ? $template_url : $template_path) . "/" . array_shift($file);
                        break;
                    case 'core':
                        $files['css'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                    default:
                        $files['css'][] = ($returnUrls === true ? $basehttp : $basepath) . "/" . array_shift($file);
                        break;
                }
            }
        }
        if (is_array($parsedInfo['css_mobile_print'])) {
            foreach ($parsedInfo['css_mobile_print'] as $file) {
                switch (key($file)) {
                    case 'theme':
                        $files['css_print'][] = ($returnUrls === true ? $template_url : $template_path) . "/" . array_shift($file);
                        break;
                    case 'core':
                        $files['css_print'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                    default:
                        $files['css_print'][] = ($returnUrls === true ? $basehttp : $basepath) . "/" . array_shift($file);
                        break;
                }
            }
        }
        if (is_array($parsedInfo['js_mobile'])) {
            foreach ($parsedInfo['js_mobile'] as $file) {
                switch (key($file)) {
                    case 'theme':
                        $files['js'][] = ($returnUrls === true ? $template_url : $template_path) . "/" . array_shift($file);
                        break;
                    case 'core':
                        $files['js'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                    default:
                        $files['js'][] = ($returnUrls === true ? $basehttp : $basepath) . "/" . array_shift($file);
                        break;
                }
            }
        }
    } else {
        if (is_array($parsedInfo['css'])) {
            foreach ($parsedInfo['css'] as $file) {
                switch (key($file)) {
                    case 'theme':
                        $files['css'][] = ($returnUrls === true ? $template_url : $template_path) . "/" . array_shift($file);
                        break;
                    case 'core':
                        $files['css'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                    default:
                        $files['css'][] = ($returnUrls === true ? $basehttp : $basepath) . "/" . array_shift($file);
                        break;
                }
            }
        }
        if (is_array($parsedInfo['css_print'])) {
            foreach ($parsedInfo['css_print'] as $file) {
                switch (key($file)) {
                    case 'theme':
                        $files['css_print'][] = ($returnUrls === true ? $template_url : $template_path) . "/" . array_shift($file);
                        break;
                    case 'core':
                        $files['css_print'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                    default:
                        $files['css_print'][] = ($returnUrls === true ? $basehttp : $basepath) . "/" . array_shift($file);
                        break;
                }
            }
        }
        if (is_array($parsedInfo['js'])) {
            foreach ($parsedInfo['js'] as $file) {
                switch (key($file)) {
                    case 'theme':
                        $files['js'][] = ($returnUrls === true ? $template_url : $template_path) . "/" . array_shift($file);
                        break;
                    case 'core':
                        $files['js'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                    default:
                        $files['js'][] = ($returnUrls === true ? $basehttp : $basepath) . "/core/" . array_shift($file);
                        break;
                }
            }
        }
    }

    return $files;
}

function aggregateFiles($files, $type, $group = '', $refreshMobile = false) {
    global $isMobile, $enableMobile, $misc_path, $conf;

    $mobile = (($enableMobile && $isMobile) || $refreshMobile === true) ? '_m' : '';
    $print = ($group == 'print') ? '_print' : '';

    @mkdir("$misc_path");
    chmod("$misc_path", 0777);
    @mkdir("$misc_path/$type");
    chmod("$misc_path/$type", 0777);

    $tmp_filepath = "$misc_path/$type/tmp$mobile$print.$type";
    $tmp = fopen($tmp_filepath, 'w');
    if (is_array($files)) {
        if ($type == 'css') {
            fwrite($tmp, '@charset "UTF-8";');
            fwrite($tmp, "\n");
        }
        foreach ($files as $file) {
            if (file_exists($file)) {
                $contents = file_get_contents($file);
                if ($type == 'css') {
                    fixCssUrls($file, $contents);
                    compressCssContent($contents);
                } elseif ($type == 'js') {
                    compressJsContent($contents);
                }
                fwrite($tmp, $contents);
                fwrite($tmp, "\n");
            }
        }
    }
    fclose($tmp);

    $filehash = getVariable('filehash' . $mobile . '_' . $conf['template_name']);
    $filepath = "$misc_path/$type/$filehash$mobile$print.$type";

    $contents = file_get_contents($tmp_filepath);
    $handle = fopen($filepath, 'w');
    fwrite($handle, $contents);
    fclose($handle);
    chmod($filepath, 0777);

    if (function_exists('gzencode')) {
        $handle_gz = fopen("$filepath.gz", 'w');
        fwrite($handle_gz, gzencode($contents, 9));
        fclose($handle_gz);
        chmod("$filepath.gz", 0777);
    }

    @unlink($tmp_filepath);

    return $filehash;
}

function compressCssContent(&$content) {
    $content = preg_replace("!/\*[^*]*\*+([^/][^*]*\*+)*/!", "", $content);
    $content = strtr($content, array(
        "\r\n" => " ",
        "\r\r" => " ",
        "\n" => " ",
        "\t" => "",
        "   " => " ",
        "  " => " ",
        "; -" => ";-",
        "; " => ";",
        " }" => "}",
        "} " => "}",
        "{ " => "{",
        " {" => "{",
        ", ." => ",.",
        ": " => ":",
        "@media" => "@media ",
    ));
}

function compressJsContent(&$content) {
    $content = preg_replace('#/\*[^*]*\*+([^/][^*]*\*+)*/#', "", $content);
    $content = preg_replace('#\s+//.*#', "", $content);
    $content = strtr($content, array(
        "   " => " ",
        "  " => " ",
        "; -" => ";-",
        "; " => ";",
        ",\n" => ",",
        " }" => "}",
        "} " => "}",
        "{ " => "{",
        " {" => "{",
        "\n}" => "}",
        "\t" => "",
    ));
}

function fixCssUrls($filepath, &$content) {
    global $basehttp, $basepath;

    $file = substr($filepath, 0, strrpos($filepath, '/') + 1);
    $collection = extractCssUrls($content);
    if (count($collection) > 0 && isset($collection['property'])) {
        foreach ($collection['property'] as $item) {
            $new_url = $basehttp . str_replace($basepath, '', $file);
            $dirs_count = substr_count($item, '../');
            $url_parts = explode('/', $new_url);
            $new_url = implode('/', array_slice($url_parts, 0, count($url_parts) - $dirs_count - 1)) . '/' . str_replace('../', '', $item);
            $content = str_replace($item, $new_url, $content);
        }
    }
}

function extractCssUrls($content) {
    $urls = array();

    preg_match_all('/url\(([\s])?[^data\:]+?([\"|\'])?(.*?)([\"|\'])?([\s])?\)/i', $content, $matches, PREG_PATTERN_ORDER);

    if (count($matches[0]) > 0) {
        foreach ($matches[0] as $match) {
            if (!empty($match) && strpos($match, 'http://') === false && strpos($match, 'https://') === false) {
                $urls['property'][] = str_replace(array("url('", 'url("', "url(", "')", '")', ")"), '', $match);
            }
        }
    }

    return $urls;
}

function getThemeSettings() {
    global $isMobile, $enableMobile, $misc_path, $misc_url, $conf;

    if ($conf['compress_css_js']) {
        if ($enableMobile && $isMobile) {
            // files aggregated for mobile site
            $filehash = getVariable('filehash_m_' . $conf['template_name'], uniqid('', true));
            $themeSettings = array(
                'cssFilePath' => "$misc_path/css/" . $filehash . "_m.css",
                'cssFileUrl' => "$misc_url/css/" . $filehash . "_m.css",
                'cssPrintFilePath' => "$misc_path/css/" . $filehash . "_m_print.css",
                'cssPrintFileUrl' => "$misc_url/css/" . $filehash . "_m_print.css",
                'jsFilePath' => "$misc_path/js/" . $filehash . "_m.js",
                'jsFileUrl' => "$misc_url/js/" . $filehash . "_m.js",
            );
        } else {
            // files aggregated for desktop site
            $filehash = getVariable('filehash_' . $conf['template_name'], uniqid('', true));
            $themeSettings = array(
                'cssFilePath' => "$misc_path/css/" . $filehash . ".css",
                'cssFileUrl' => "$misc_url/css/" . $filehash . ".css",
                'cssPrintFilePath' => "$misc_path/css/" . $filehash . "_print.css",
                'cssPrintFileUrl' => "$misc_url/css/" . $filehash . "_print.css",
                'jsFilePath' => "$misc_path/js/" . $filehash . ".js",
                'jsFileUrl' => "$misc_url/js/" . $filehash . ".js",
            );
        }
    } else {
        $themeSettings = readTemplateInfo(true);
    }

    return $themeSettings;
}

/**
 * Parse .info files in templates directories
 */
function parseInfoFile($filename) {
    $info = array();

    if (!file_exists($filename)) {
        return $info;
    }

    $data = file_get_contents($filename);
    if (preg_match_all('
                @^\s*                           # Start at the beginning of a line, ignoring leading whitespace
                ((?:
                  [^=;\[\]]|                    # Key names cannot contain equal signs, semi-colons or square brackets,
                  \[[^\[\]]*\]                  # unless they are balanced and not nested
                )+?)
                \s*=\s*                         # Key/value pairs are separated by equal signs (ignoring white-space)
                (?:
                  ("(?:[^"]|(?<=\\\\)")*")|     # Double-quoted string, which may contain slash-escaped quotes/slashes
                  (\'(?:[^\']|(?<=\\\\)\')*\')| # Single-quoted string, which may contain slash-escaped quotes/slashes
                  ([^\r\n]*?)                   # Non-quoted string
                )\s*$                           # Stop at the next end of a line, ignoring trailing whitespace
                @msx', $data, $matches, PREG_SET_ORDER)) {
        foreach ($matches as $match) {
            // Fetch the key and value string
            $i = 0;
            foreach (array('key', 'value1', 'value2', 'value3') as $var) {
                $$var = isset($match[++$i]) ? $match[$i] : '';
            }
            $value = stripslashes(substr($value1, 1, -1)) . stripslashes(substr($value2, 1, -1)) . $value3;

            // Parse array syntax
            $keys = preg_split('/\]?\[/', rtrim($key, ']'));
            $last = array_pop($keys);
            $parent = &$info;

            // Create nested arrays
            foreach ($keys as $key) {
                if ($key == '') {
                    $key = count($parent);
                }
                if (!isset($parent[$key]) || !is_array($parent[$key])) {
                    $parent[$key] = array();
                }
                $parent = &$parent[$key];
            }

            // Handle PHP constants.
            if (isset($constants[$value])) {
                $value = $constants[$value];
            }

            // Insert actual value
            if ($last == '') {
                $last = count($parent);
            }
            $parent[$last] = $value;
        }
    }
    return $info;
}

function getHeadContents($type) {
    global $themeSettings, $isDebug;

    $html = '';
    switch ($type) {
        case 'css':
            if ($isDebug || !file_exists($themeSettings['cssFilePath'])) {
                foreach ($themeSettings['css'] as $element) {
                    $html .= '<link rel="stylesheet" media="screen" href="' . $element . '">' . "\n";
                }
            } else {
                $html .= '<link rel="stylesheet" media="screen" href="' . $themeSettings['cssFileUrl'] . '">' . "\n";
            }
            break;
        case 'css_print':
            if ($isDebug || !file_exists($themeSettings['cssPrintFilePath'])) {
                foreach ($themeSettings['css_print'] as $element) {
                    $html .= '<link rel="stylesheet" media="print" href="' . $element . '">' . "\n";
                }
            } else {
                $html .= '<link rel="stylesheet" media="print" href="' . $themeSettings['cssPrintFileUrl'] . '">' . "\n";
            }
            break;
        case 'js':
            if ($isDebug || !file_exists($themeSettings['jsFilePath'])) {
                foreach ($themeSettings['js'] as $element) {
                    $html .= '<script src="' . $element . '"></script>' . "\n";
                }
            } else {
                $html .= '<script src="' . $themeSettings['jsFileUrl'] . '"></script>' . "\n";
            }
            break;
    }

    return $html;
}

function cleanupThemeFiles($check_only = false) {
    global $misc_path, $conf;

    $data = array(
        'amount_outdated' => 0,
    );
    if (is_dir("$misc_path/css")) {
        $files = (array) getFileList("$misc_path/css", array('css', 'gz'));
        foreach ($files as $file) {
            if (filemtime($file) <= (time() - ($conf['outdated_files_diff'] * 3600))) {
                $data['files'][] = $file;
                $data['amount_outdated'] ++;
            }
        }
    }
    if (is_dir("$misc_path/js")) {
        $files = (array) getFileList("$misc_path/js", array('js', 'gz'));
        foreach ($files as $file) {
            if (filemtime($file) <= (time() - ($conf['outdated_files_diff'] * 3600))) {
                $data['files'][] = $file;
                $data['amount_outdated'] ++;
            }
        }
    }
    if (is_array($data['files']) && $check_only !== true) {
        $count = 0;
        foreach ($data['files'] as $file) {
            chmod($file, 0777);
            shell_exec("rm -f $file");
            $count++;
        }
        setMessage("$count outdated CSS/JS cached files removed");
    }

    return $data;
}

function getTimezones() {
    $regions = array(
        'Africa' => DateTimeZone::AFRICA,
        'America' => DateTimeZone::AMERICA,
        'Antarctica' => DateTimeZone::ANTARCTICA,
        'Asia' => DateTimeZone::ASIA,
        'Atlantic' => DateTimeZone::ATLANTIC,
        'Australia' => DateTimeZone::AUSTRALIA,
        'Europe' => DateTimeZone::EUROPE,
        'Indian' => DateTimeZone::INDIAN,
        'Pacific' => DateTimeZone::PACIFIC
    );

    $zones = array();
    foreach ($regions as $name => $mask) {
        $tzlist[strtolower($name)] = DateTimeZone::listIdentifiers($mask);
        foreach ($tzlist[strtolower($name)] as $item) {
            $zones[strtolower($name)][$item] = $item;
        }
    }

    return $zones;
}

function getThemesList() {
    global $basepath;

    $valid_themes = array();
    $themes = (array) scandir("$basepath/templates");
    unset($themes[0], $themes[1]);
    foreach ($themes as $item) {
        $themeInfo = parseInfoFile("$basepath/templates/$item/template.info");
        if (!empty($themeInfo)) {
            $valid_themes[$item] = "($item) " . $themeInfo['name'];
        }
    }

    return $valid_themes;
}

function getMemberUploadStatus() {
    global $conf;

    if (!isset($_SESSION['user'])) {
        return false;
    }

    if ($_SESSION['user']['user_level'] > 0 && $conf['allow_uploads_premium_member']) {
        return true;
    } elseif ($_SESSION['user']['user_level'] == 0 && $conf['allow_uploads_free_member']) {
        return true;
    }

    return false;
}

function secureEval($input = '') {
    $functions = array(
        'assert',
        'include',
        'include_once',
        'require',
        'require_once',
        'call_user_func',
        'call_user_func_array',
        'curl_init',
        'exec',
        'fpassthru',
        'fsockopen',
        'file',
        'fopen',
        'mail',
        'exec',
        'system',
        'sockopen',
        'shell_exec',
        'socket_connect',
    );
    $original = ini_get('disable_functions');
    ini_set('disable_functions', implode(', ', $functions));
    eval("?>" . $input);
    ini_set('disable_functions', $original);
}

function natsort2d(&$aryInput) {
    $aryTemp = $aryOut = array();
    foreach ($aryInput as $key => $value) {
        reset($value);
        $aryTemp[$key] = current($value);
    }
    natsort($aryTemp);
    foreach ($aryTemp as $key => $value) {
        $aryOut[] = $aryInput[$key];
    }
    $aryInput = $aryOut;
}
