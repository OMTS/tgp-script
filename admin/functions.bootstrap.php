<?php

if (!isset($site_id)) {
    $site_id = 1;
}

/* LOAD SYSTEM VARIABLES */
$GLOBALS['conf'] = array();
$conf['dbprefix'] = $dbprefix;

$_variables = dbQuery("SELECT `variable`, `value`, `type` FROM `variables` WHERE `site_id` = 1", false);
if ($site_id > 1) {
    $_vars = array();
    $_variables_instance = dbQuery("SELECT `variable`, `value`, `type` FROM `variables` WHERE `site_id` = '" . (int) $site_id . "'", false);
    if (is_array($_variables_instance)) {
        foreach ($_variables as $var) {
            $var['value'] = unserialize($var['value']);
            $_vars[$var['variable']] = $var['value'];
        }
    }
}
if (is_array($_variables)) {
    foreach ($_variables as $var) {
        $var['value'] = unserialize($var['value']);
        $GLOBALS['conf'][$var['variable']] = $var['value'];
    }
    if (isset($_vars) && is_array($_vars)) {
        $GLOBALS['conf'] += $_vars;
    }
    unset($var);
    $sitename = $conf['sitename'];
    $default_timezone = $conf['default_timezone'];
    $template_name = $conf['template_name'];
    $template_path = "$basepath/templates/$template_name";
    $template_url = "$basehttp/templates/$template_name";
    $results_per_row = $conf['results_per_row'];
    if (isset($default_timezone) && $default_timezone != "") {
        date_default_timezone_set($default_timezone);
    }
}
unset($_variables);
preg_match('/[0-9]\.[0-9]+\.[0-9]+/', mysql_get_server_info(), $version);
$conf['mysql_version'] = (float) $version[0];
if (isset($conf['cache_memcached_server']) && !isset($conf['cache_memcache_server'])) {
    $conf['cache_memcache_server'] = $conf['cache_memcached_server'];
    $conf['cache_memcache_port'] = $conf['cache_memcached_port'];
}
/* --------------------- */

/* LOAD IMAGES PRESETS */
$conf['images_presets'] = array();
$_presets = dbQuery("SELECT * FROM `images_presets`", true, 60, 'table.images_presets');
if (is_array($_presets)) {
    foreach ($_presets as $preset) {
        $conf['images_presets'][$preset['group']][$preset['preset']] = array(
            'width' => $preset['width'],
            'height' => $preset['height'],
        );
    }
    unset($preset);
}
unset($_presets);
/* --------------------- */

if (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    if (isset($_SESSION['user'])) {
        $_SESSION['user']['_timestamp'] = time();
        $session_reload = dbRow("SELECT * FROM `users` WHERE `record_num` = '" . (int) $_SESSION['user']['record_num'] . "'");
        if (is_array($session_reload)) {
            if ($session_reload['enabled'] == 0) {
                dbUpdate('users', array('session_reload' => 0, 'record_num' => $_SESSION['user']['record_num']));
                unset($_SESSION['user']);
                setMessage('Your account has been blocked.', 'error');
                header("Location: $basehttp");
                exit;
            } elseif ($session_reload['session_reload'] == 1) {
                updateUserSession();
                dbUpdate('users', array('session_reload' => 0, 'record_num' => $_SESSION['user']['record_num']));
            }
        } else {
            unset($_SESSION['user']);
            setMessage('Your account has been removed.', 'error');
            header("Location: $basehttp");
            exit;
        }
    }
}

if (isset($_SESSION['admin'])) {
    $_SESSION['admin']['_timestamp'] = time();
}

if (isset($_SESSION['user']['ip']) && $_SESSION['user']['ip'] != $_SERVER['REMOTE_ADDR']) {
    unset($_SESSION['user']);
    header("Location: $basehttp/login");
    exit;
}