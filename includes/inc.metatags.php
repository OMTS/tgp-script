<?

$mode = $_GET['mode'];

$meta_settings = dbRow("SELECT * FROM `metatags` WHERE `controller` = '" . mysql_real_escape_string($thisController) . "' AND `mode` = '" . mysql_real_escape_string($mode) . "' AND `site_id` = '" . (int) $site_id . "'");

$conf['metatags'] = (array) $_meta + array(
    'meta_title' => $conf['meta_title'],
    'meta_description' => $conf['meta_description'],
    'meta_keywords' => $conf['meta_keywords'],
    'meta_robots' => $conf['meta_robots'],
    'sitename' => $conf['sitename'],
);

/* Override certain metatags for specific index modes */
if ($thisController === 'index') {
    switch ($mode) {
        case 'search':
            $conf['metatags']['search_string'] = htmlentities(str_replace('-', ' ', strip_tags(filter_input(INPUT_GET, 'q', FILTER_SANITIZE_STRING))), ENT_QUOTES, 'UTF-8');
            break;
        case 'channel':
            $_channel = dbRow("SELECT `name`, `metakw`, `metadesc` FROM `niches` WHERE `record_num` = '" . filter_input(INPUT_GET, 'channel', FILTER_SANITIZE_NUMBER_INT) . "'");
            $conf['metatags']['channel_name'] = htmlentities(ucwords($_channel['name']), ENT_QUOTES, 'UTF-8');
            $conf['metatags']['description'] = truncate($_channel['metadesc'], 156);
            $conf['metatags']['keywords'] = $_channel['metakw'];
            break;
        case 'paysite':
            $_paysite = dbRow("SELECT `name` FROM `paysites` WHERE `record_num` = '" . filter_input(INPUT_GET, 'paysite', FILTER_SANITIZE_NUMBER_INT) . "'");
            $conf['metatags']['paysite_name'] = htmlentities(ucwords($_paysite['name']), ENT_QUOTES, 'UTF-8');
            $conf['metatags']['description'] = truncate($_paysite['metadesc'], 156);
            $conf['metatags']['keywords'] = $_paysite['metakw'];
            break;
        case 'uploads-by-user':
            $_user = dbRow("SELECT `username` FROM `users` WHERE `record_num` = '" . filter_input(INPUT_GET, 'user', FILTER_SANITIZE_NUMBER_INT) . "'");
            $conf['metatags']['user_name'] = htmlentities(ucwords($_user['username']), ENT_QUOTES, 'UTF-8');
            break;
    }
}

$conf['metatags']['description'] = htmlentities(truncate(trim($conf['metatags']['description'] . ' ' . $conf['metatags']['meta_description']), 156), ENT_QUOTES, 'UTF-8');
$conf['metatags']['keywords'] = htmlentities(truncate(trim($conf['metatags']['keywords'] . ' ' . $conf['metatags']['meta_keywords']), 156), ENT_QUOTES, 'UTF-8');

$replacement = array();
foreach ($conf['metatags'] as $metatag_key => $metatag_value) {
    $replacement['{{' . strtoupper($metatag_key) . '}}'] = $metatag_value;
}

if (is_array($meta_settings)) {
    foreach ($meta_settings as &$meta) {
        $meta = strtr($meta, $replacement);
    }
    $title = $meta_settings['meta_title'];
    $headertitle = $meta_settings['header_title'];
    $metadescription = $meta_settings['meta_description'];
    $metakeywords = $meta_settings['meta_keywords'];
    $metarobots = $meta_settings['robots'];
} else {
    $title = $conf['meta_title'];
    $headertitle = $conf['default_title'];
    $metadescription = $conf['meta_description'];
    $metakeywords = $conf['meta_keywords'];
    $metarobots = $conf['meta_robots'];
}