# Mechbunny TGP Script
#### A Modern Approach to Image Galleries

![product-2-intro-img.png](product-2-intro-img.png)

Mechbunny TGP Script is a gallery hosting script written in PHP5. It is 
written from the ground up to be a complete content management system 
for photo gallery websites, both adult and mainstream. The galleries 
can be rotated based on productivity, and comes with many useful 
features such as thumbnail cropping, multiple thumbnail size support, 
gallery scraping, automatic publishing, a model directory and a very 
easy to use template system. It is written on the same framework and 
templating system as our very popular and easy to use tube script, so 
you can be sure it will support your website's traffic.

The script processes your thumbnails using imagemagick, so you can 
easily sharpen or apply effects to them by editing the command line. 
Images are automatically cropped and can be re-cropped manually via the 
admin area. It allows users to upload galleries (can be disabled) and 
has a verification queue for all uploaded content so you can review it 
before publishing. The script is compatible with any external trading 
script.

As with any of our scripts, we are available and willing to perform 
customizations for clients.

# Main features

## High-Performance
Built on Mechbunny's Tube Script framework

## Simplicity
Fully template driven (simple php include system)

## 6 Included Templates
Just add in your logo, or use them as a starting point for a custom 
design

## Import images
Bulk Gallery Importer

## Cache
Two caching systems - memcached and filesystem

## Customizable
Customization has never been easier! Our software comes 99% unencoded 
which allows you to edit functionality to suit your individual needs.

## Thumbnail
Automatic Thumbnailing in various definable sizes

## Performance
We take it to a whole new level. Our software has proven itself on 
websites generating millions of pageviews daily.

![p_tgp-1.png](p_tgp-1.png) ![p_tgp-2.png](p_tgp-2.png) ![p_tgp-3.png](p_tgp-3.png) ![p_tgp-4.png](p_tgp-4.png)

# Additional features


* Search Engine (with SE friendly urls), sortable by Most Relevant, Newest, Top Rated, Most Viewed)
* Image gallery slideshow
* Search engine friendly URLs
* User gallery uploading
* User registration
* User login
* User favorites
* Model Directory
* Fully Responsive Default Template
* 98/100 PageSpeed insights mobile usability score

* Automatic site updating (publish galleries automatically every x minutes)
* Sorting into categories
* Sorting into models
* Sorting into pornstars
* Upload via CSV Import (with or without hotlinking, allows embedded content)
* Upload via FTP Import
* Gallery Scraper (grab galleries from existing urls)
* Gallery approval before publishing
* Publishing queue

* Static page management
* All PC features available on mobile
* User community features
* User Profiles
* User Email Notifications
* User Friends List
* User Avatars
* User Profile Comments/Wall
* User Favorites
* User Commenting on Photo Gallery Pages
* User photo uploads with progress bar

* Self hosted Galleries
* Partner upload accounts
* Built in advertising system that supports iframes, javascript, banners, text, and flash ads , etc.
* Automatic redirection of mobile devices to mobile version, or to external URL
* Configure thumbnail width/height and compression settings.
* Custom user fields
* Custom fields on model directory

# 6 Free Templates Included!
[Default](http://tgp.mechbunny.com/load/default) | 
[AngelTGP](http://tgp.mechbunny.com/load/angeltgp) | 
[BigSam](http://tgp.mechbunny.com/load/bigsam) | 
[BeautyTGP](http://tgp.mechbunny.com/load/beautytgp) | 
[NewPhotos](http://tgp.mechbunny.com/load/newphotos) | 
[TGPStar](http://tgp.mechbunny.com/load/tgpstar)

# Script Requirements

Apache/Lighttpd/Nginx Web Server
PHP 5.3 or Higher
MySQL
[ImageMagick](http://www.imagemagick.org/script/index.php) 6.7.7 or Higher
[IonCube Loader](https://www.ioncube.com/)
`short_open_tag must` be enabled
